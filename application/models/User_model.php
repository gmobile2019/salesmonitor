<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model
{
   
    function retrieve_graph_data($id){
 
        $data=array();
        $date=date('Y-m-d');
        $day=date('D');
        $i=1;
        while($i <= 4){
            
            $dates=$this->calculate_week_dates($date,$day);
            $dt_values=$this->graphical_data($dates,$id);
             $inner_values=array();
             $inner_values[0]=$dates['Date1'].'-'.$dates['Date2'];
           
             $inner_values=$inner_values+$dt_values;
             $data[]=$inner_values;
           
            $date=$dates['Date2'];
            $day=null;
            $i++;
        }
             
       
        return $data;
    }
    
    function graphical_data($dates,$id){
        $data=array();
        
        if($id == 1){
            $field='CLOSE';
            $vertical_data=array(20,40,60,80);
        }else{
            $field='STAGE';
            $vertical_data=array(0,1,2,3);
        }
        
        $i=1;
        $pjcts=$this->distinct_projects();
        foreach($vertical_data as $key=>$value){
            
              foreach($pjcts as $ky=>$val){
                  
                $var=null;
                $cnt=$this->db->query("SELECT COUNT(ID) as id_count FROM sales_activity WHERE ACTIONDATE>='".$dates['Date2']." 00:00:00' AND ACTIONDATE <'".$dates['Date1']." 23:59:59' AND $field='$value' AND PROJECTID='$val->ID'")->row();
                //echo "SELECT COUNT(ID) as id_count FROM sales_activity WHERE ACTIONDATE>='".$dates['Date2']." 00:00:00' AND ACTIONDATE <'".$dates['Date1']." 23:59:59' AND $field='$value' AND PROJECTID='$val->ID'";exit;
                if($cnt->id_count > 0){
                    $var=$value;
                }
                
                $data[$i++]=$var;
              }
        }
        
        return $data;
    }
    
    function distinct_projects(){
        
        return $this->db->query("SELECT ID,NAME FROM project_details WHERE STATUS=0 ORDER BY NAME ASC")->result();
    }
    
    function calculate_week_dates($date,$day){
        
        if($day == 'Mon'){
            $date1=date('Y-m-d');
            $date2=Date($date, strtotime("-3 days"));
        }else if($day == 'Tue'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-4 days"));
            
        }else if($day == 'Wed'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-5 days"));
        }else if($day == 'Thu'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-6 days"));
        }else if($day == 'Fri'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-7 days"));
        }else if($day == 'Sat'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-1 days"));
        }else if($day == 'Sun'){
            $date1=date('Y-m-d');
            $date2=Date(('Y-m-d'), strtotime("-2 days"));
        }else{
            
            $date1=date_create($date);
            date_sub($date1, date_interval_create_from_date_string('7 days'));
            $date2=date_format($date1, 'Y-m-d');
        }
        
        return array('Date1'=>$date,'Date2'=>$date2);
    }
    
    function projects_info_count($name,$org,$category){
        
        if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND ORGANIZATION LIKE '%$org%'";
        }
        
        if($category <> null){
            
            $where .=" AND CATEGORY='$category'";
        }
        
        return count($this->db->query("SELECT ID,NAME,ORGANIZATION,"
                . "DESCRIPTION,TOTALVALUE,PERFOMANCEDURATION,"
                . "YEAR1,YEAR2,YEAR3,YEAR4,STATUS "
                . "FROM project_details "
                . "WHERE ID<>0 $where ORDER BY ID ASC")->result());
    }
    
    function projects_info($name,$org,$category,$page,$limit){
       
       if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND ORGANIZATION LIKE '%$org%'";
        }
        
        if($category <> null){
            
            $where .=" AND CATEGORY='$category'";
        }
        
        return $this->db->query("SELECT ID,NAME,ORGANIZATION,"
                . "DESCRIPTION,TOTALVALUE,PERFOMANCEDURATION,"
                . "YEAR1,YEAR2,YEAR3,YEAR4,STATUS,CATEGORY "
                . "FROM project_details "
                . "WHERE ID<>0 $where ORDER BY ID ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function create_project($array,$id){
        
        if($id == null){
          $array['CREATEDBY']=$this->session->userdata('user_id');
          $this->db->insert('project_details',$array);
          return $this->db->insert_id();
        } 
          $array['MODIFIEDBY']=$this->session->userdata('user_id');
          $array['MODIFIEDON']=date('Y-m-d H:i:s');
         $this->db->update('project_details',$array,array('ID'=>$id));
         return $id;
    }
    
    function add_project_products_services($array,$id){
             
             $this->db->delete('project_products_services',array('PROJECTID'=>$id));
      return $this->db->insert_batch('project_products_services',$array);
    }
    
    function projects($id){
        
        if($id <> null){
            $where .=" AND ID='$id'";
        }
        
        return $this->db->query("SELECT ID,NAME,ORGANIZATION,DESCRIPTION,TOTALVALUE,PERFOMANCEDURATION,YEAR1,YEAR2,YEAR3,YEAR4 FROM project_details WHERE STATUS=0 $where ORDER BY ID ASC")->result();
    }
    
    function projects_user_specific($user_id){
        
         return $this->db->query("SELECT ID,NAME,ORGANIZATION,DESCRIPTION,TOTALVALUE,PERFOMANCEDURATION,YEAR1,YEAR2,YEAR3,YEAR4 FROM project_details WHERE STATUS=0 AND SALESREP='$user_id' ORDER BY ID ASC")->result();
    }
    
    function project_contacts($id,$projectid){
        
        if($id <> null){
            $where .=" AND ID='$id'";
        }
        
         if($projectid <> null){
             
            $where .=" AND PROJECTID='$projectid'";
        }
        
        return $this->db->query("SELECT ID,PROJECTID,NAME,EMAIL,PHONE,TITLE FROM contacts WHERE ID <>0 $where ORDER BY ID ASC")->result();
        
    }
    
    function add_contact($array,$id){
        if($id == null){
          
          $this->db->insert('contacts',$array);
          return $this->db->insert_id();
        } 
          
         $this->db->update('contacts',$array,array('ID'=>$id));
         return $id;
    }
    
    function project_details($project_id){
        
        return $this->db->query("SELECT ID,NAME,CATEGORY,ORGANIZATION,DESCRIPTION,TOTALVALUE,PERFOMANCEDURATION,YEAR1,YEAR2,YEAR3,YEAR4,SALESREP,STATUS FROM project_details WHERE ID='$project_id'")->row();
    }
    
    function get_duration($id){
        
        if($id <> null){
            $where .=" WHERE ID='$id'";
        }
        
        return $this->db->query("SELECT ID,NAME,DURATION FROM perfomance_durations $where ORDER BY ID ASC")->result();
    }
    
    function project_products_services($id,$projectid){
         if($id <> null){
            $where .=" AND ID='$id'";
        }
        
         if($projectid <> null){
             
            $where .=" AND PROJECTID='$projectid'";
        }
       
        return $this->db->query("SELECT PROJECTID,PRODUCTID,ID FROM project_products_services WHERE ID <>0 $where ORDER BY ID ASC")->result();
    }
    
    function project_products($projectid){
        
        $array=array();
        $products=$this->db->query("SELECT PRODUCTID FROM project_products_services WHERE PROJECTID='$projectid' ORDER BY ID ASC")->result();
        
        foreach($products as $key=>$value){
            $array[]=$value->PRODUCTID;
        }
        
        return $array;
    }
    
    function operational_details($id,$projectid){
        if($id <> null){
            $where .=" AND ID='$id'";
        }
        
         if($projectid <> null){
             
            $where .=" AND PROJECTID='$projectid'";
        }
        
        return $this->db->query("SELECT PROJECTID,RISKS,ID,COMPETITION FROM operational_details WHERE ID <>0 $where ORDER BY ID ASC")->result();
    }
    
    function add_operational_detail($array){
        
                $this->db->insert('operational_details',$array);
        return $this->db->insert_id();
    }
    
    function remove_risk($id){
        
        return $this->db->update('operational_details',array('RISKS'=>null),array('ID'=>$id));
    }
    
    function remove_competition($id){
        
        return $this->db->update('operational_details',array('COMPETITION'=>null),array('ID'=>$id));
    }
    
    function save_activity($array){
        
               $this->db->insert('sales_activity',$array);
        return $this->db->insert_id();
    }
    
     function activities_info_count($project,$org,$close,$stage,$start,$end){
        
         if($project <> null){
            
           $where .=" AND project_details.NAME LIKE '%$project%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND project_details.ORGANIZATION LIKE '%$org%'";
        }
        
        if($close <> null){
            
            $where .=" AND sales_activity.CLOSE='$close'";
        }
        
        if($stage <> null){
            
            $where .=" AND sales_activity.STAGE='$stage'";
        }
        
        if($start <> null){
            
            $where .=" AND sales_activity.ACTIONDATE >= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND sales_activity.ACTIONDATE <= '$end 23:59:59'";
        }
        
        return count($this->db->query("SELECT sales_activity.PROJECTID,sales_activity.ID,"
                . "sales_activity.ACTIONDATE,sales_activity.STAGE,"
                . "sales_activity.CLOSE,sales_activity.COMMENTS,"
                . "project_details.ORGANIZATION,project_details.NAME as PNAME "
                . "FROM sales_activity INNER JOIN project_details "
                . "ON sales_activity.PROJECTID=project_details.ID "
                . "$where ORDER BY sales_activity.ACTIONDATE DESC")->result());
    }
    
    function activities_info($project,$org,$close,$stage,$start,$end,$page,$limit){
       
       if($project <> null){
            
           $where .=" AND project_details.NAME LIKE '%$project%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND project_details.ORGANIZATION LIKE '%$org%'";
        }
        
        if($close <> null){
            
            $where .=" AND sales_activity.CLOSE='$close'";
        }
        
        if($stage <> null){
            
            $where .=" AND sales_activity.STAGE='$stage'";
        }
        
        if($start <> null){
            
            $where .=" AND sales_activity.ACTIONDATE >= '$start 00:00:00'";
        }
        
        if($end <> null){
            
            $where .=" AND sales_activity.ACTIONDATE <= '$end 23:59:59'";
        }
        
        return $this->db->query("SELECT sales_activity.PROJECTID,sales_activity.ID,"
                . "sales_activity.ACTIONDATE,sales_activity.STAGE,"
                . "sales_activity.CLOSE,sales_activity.COMMENTS,"
                . "project_details.ORGANIZATION,project_details.NAME as PNAME "
                . "FROM sales_activity INNER JOIN project_details "
                . "ON sales_activity.PROJECTID=project_details.ID "
                . "$where ORDER BY sales_activity.ACTIONDATE DESC "
                . "LIMIT $page,$limit")->result();
    }
    
    function close_project($projectid){
        
        return $this->db->update('project_details',array('STATUS'=>1),array('ID'=>$projectid));
    }
    
    function activate_deactivate_sender($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->db->update('sms_sender_id',array('STATUS'=>$new_status),array('ID'=>$id));
    }
    
    function address($id){
        
        if($id <> null){
            $where =" WHERE ID='$id'";
        }
        
        return $this->db->query("SELECT ID,PHONE1,PHONE2,PHONE3,NAME,ORGANIZATION,EMAIL,PHY_ADDRESS,COMMENTS FROM address_book $where ORDER BY ORGANIZATION,NAME ASC")->result();
    }
    
    function save_address($data,$id){
        
        if($id == null){
          
          $this->db->insert('address_book',$data);
          return $this->db->insert_id();
        } 
          
         $this->db->update('address_book',$data,array('ID'=>$id));
         return $id;
    }
    
    function address_info_count($name,$org,$phone){
        
         if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND ORGANIZATION LIKE '%$org%'";
        }
        
        if($phone <> null){
            
            $where .=" AND (PHONE1 LIKE '%$phone%' OR PHONE2 LIKE '%$phone%' OR PHONE3 LIKE '%$phone%' ) ";
        }
        
        return count($this->db->query("SELECT ID,PHONE1,PHONE2,PHONE3,"
                . "NAME,ORGANIZATION,EMAIL,PHY_ADDRESS,COMMENTS "
                . "FROM address_book WHERE ID !=0 "
                . "$where")->result());
    }
    
    function address_info($name,$org,$phone,$page,$limit){
       
       if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
       
        
        if($org <> null){
            
            $where .=" AND ORGANIZATION LIKE '%$org%'";
        }
        
        if($phone <> null){
            
            $where .=" AND (PHONE1 LIKE '%$phone%' OR PHONE2 LIKE '%$phone%' OR PHONE3 LIKE '%$phone%' ) ";
        }
        
               
        return $this->db->query("SELECT ID,PHONE1,PHONE2,PHONE3,"
                . "NAME,ORGANIZATION,EMAIL,PHY_ADDRESS,COMMENTS "
                . "FROM address_book WHERE ID !=0 "
                . "$where ORDER BY ORGANIZATION,NAME ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function remove_address($id){
       
        return $this->db->query("DELETE FROM address_book WHERE ID='$id'");
    }
    
    function business_categories_count($category){
        
         if($category <> null){
            
           $where ="WHERE CATEGORY LIKE '%$category%'"; 
        }
       
        
        return count($this->db->query("SELECT ID,CATEGORY,DESCRIPTION "
                . "FROM business_categories "
                . "$where ORDER BY CATEGORY ASC")->result());
    }
    
    function business_categories($category,$page,$limit){
       
       if($category <> null){
            
           $where ="WHERE CATEGORY LIKE '%$category%'"; 
        }
        
        return $this->db->query("SELECT ID,CATEGORY,DESCRIPTION "
                . "FROM business_categories "
                . "$where ORDER BY CATEGORY ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function business_category($id){
        if($id <> null){
            
           $where ="WHERE ID='$id'"; 
        }
        
        return $this->db->query("SELECT ID,CATEGORY,DESCRIPTION "
                . "FROM business_categories "
                . "$where ORDER BY CATEGORY ASC")->result();
    }
    
    function save_business_category($data,$id){
       
        if($id <> null){
            
            return $this->db->update('business_categories',$data,array('ID'=>$id));
        }
        
        return $this->db->insert('business_categories',$data);
    }
}