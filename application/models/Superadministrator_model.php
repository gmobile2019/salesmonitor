<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Superadministrator_model extends CI_Model
{
  
    
    public function __construct()
	{
		parent::__construct();
		
		$this->load->config('ion_auth', TRUE);
		$this->store_salt      = $this->config->item('store_salt', 'ion_auth');
		$this->salt_length     = $this->config->item('salt_length', 'ion_auth');
		
        }
   
    
    function current_user_info(){
        
        return $this->db->query("SELECT users.FIRST_NAME,users.MIDDLE_NAME,users.LAST_NAME FROM users WHERE users.ID='".$this->session->userdata('user_id')."'")->row();
        
    }
    
    function profile_data(){
        
        return $this->db->query("SELECT users.SALT,users.PASSWORD,users.ID,users.FIRST_NAME,users.MIDDLE_NAME,users.LAST_NAME,users.USERNAME,users.EMAIL,users.MSISDN,users_groups.GROUP_ID FROM users INNER JOIN users_groups ON users.ID=users_groups.USER_ID WHERE users.ID='".$this->session->userdata('user_id')."'")->row();
    }
    
    function operators(){
        
        
           return $this->db->query("SELECT users.ID,users.FIRST_NAME,users.LAST_NAME,users_groups.GROUP_ID FROM users INNER JOIN users_groups ON users.ID=users_groups.USER_ID WHERE users_groups.GROUP_ID='3' AND COMPANY='".$this->session->userdata('company')."'")->result();
    }
    
    function get_member_info($id){
        
        if($id <> null){
            
            $where =" WHERE users.ID='$id'";
            
        }
       
        return $this->db->query("SELECT users.ID,users.FIRST_NAME,"
                . "users.MIDDLE_NAME,users.LAST_NAME,users.EMAIL,"
                . "users.MSISDN,users.USERNAME,users_groups.GROUP_ID "
                . "FROM users INNER JOIN users_groups "
                . "ON users.ID=users_groups.USER_ID $where "
                . "ORDER BY users.FIRST_NAME ASC")->result();
       
    }
    
    function get_registration_groups(){
        
        
        return $this->db->query("SELECT ID,NAME FROM groups $where ORDER BY NAME ASC")->result();
    }
    
    function check_username($username,$id){
        
        if($id <> null){
            
            $usernames=$this->db->query("SELECT COUNT(ID) username_count FROM users WHERE USERNAME LIKE '$username' AND ID<>'$id'")->row();
        }else{
            
            $usernames=$this->db->query("SELECT COUNT(ID) username_count FROM users WHERE USERNAME LIKE '$username'")->row();
        }
        
        if($usernames->username_count > 0){
            
            return FALSE;
        }
        
        return TRUE;
        
    }
    
   
    
    function system_user_registration($user,$user_group,$id,$password){
        
       if($password <> null){
           $salt= $this->store_salt ? $this->salt() : FALSE;
       
            if(!$salt){

                return FALSE;
            }

            $password= $this->hash_password($password, $salt);
            
            $user['PASSWORD']=$password;
            $user['SALT']=$salt;
       }
       
      if($id == null){
          
          $user['CREATEDON']=date('YmdHis');
          $user['CREATEDBY']=$this->session->userdata('user_id');
          $usr=$this->db->insert('users',$user);
          
          if($user){
             
              $user_group['USER_ID']=$this->db->insert_id();
              return $this->db->insert('users_groups',$user_group);
          }
          return FALSE;
      } 
              $user['MODIFIEDON']=date('YmdHis');
              $user['MODIFIEDBY']=$this->session->userdata('user_id');
          
              $this->db->update('users',$user,array('ID'=>$id));
       return $this->db->update('users_groups',$user_group,array('USER_ID'=>$id));
    }
    
    function change_password($password){
        
        $salt=$this->profile_data();
        $password=$this->hash_password($password,$salt->SALT);
        
        return $this->db->update('users',array('PASSWORD'=>$password),array('ID'=>$this->session->userdata('user_id')));
    }
    
    function salt()
    {
        
            return substr(md5(uniqid(rand(), true)), 0,$this->salt_length);
    }
    
    function hash_password($password, $salt=false)
	{
		if ($this->store_salt && $salt)
		{
			return md5($password.$salt);
		}
		else
		{
			$salt = $this->salt();
			return  $salt . substr(md5($salt . $password), 0, -$this->salt_length);
		}
	}
    
    function groups($id){
        
        if($id <> null){
            
            $where =" WHERE ID='$id'";
            
        }
        
        return $this->db->query("SELECT ID,NAME,DESCRIPTION FROM groups $where ORDER BY NAME ASC")->result();
    }
    
    function member_info_count($name){
        
        if($name <> null){
           $where .=" AND (users.FIRST_NAME LIKE '%$name%' OR users.MIDDLE_NAME LIKE '%$name%' OR users.LAST_NAME LIKE '%$name%')"; 
        }
        
        
        
        return count($this->db->query("SELECT users.ID,users.FIRST_NAME,"
                . "users.MIDDLE_NAME,users.LAST_NAME,users.EMAIL,"
                . "users.MSISDN,users.USERNAME,users_groups.GROUP_ID "
                . "FROM users INNER JOIN users_groups "
                . "ON users.ID=users_groups.USER_ID $where "
                . "ORDER BY users.FIRST_NAME ASC")->result());
    }
    
    function member_info($name,$page,$limit){
        
        if($name <> null){
           $where .=" AND (users.FIRST_NAME LIKE '%$name%' OR users.MIDDLE_NAME LIKE '%$name%' OR users.LAST_NAME LIKE '%$name%')"; 
        }
        
        
        
        return $this->db->query("SELECT users.ID,users.FIRST_NAME,"
                . "users.MIDDLE_NAME,users.LAST_NAME,users.EMAIL,"
                . "users.MSISDN,users.USERNAME,users.ACTIVE,users_groups.GROUP_ID "
                . "FROM users INNER JOIN users_groups "
                . "ON users.ID=users_groups.USER_ID $where"
                . "ORDER BY users.FIRST_NAME ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function activate_deactivate_users($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->db->update('users',array('ACTIVE'=>$new_status),array('ID'=>$id));
    }
    
     function check_product_service_name($name,$id){
         if($id <> null){
            
            $names=$this->db->query("SELECT COUNT(ID) name_count FROM products_services WHERE NAME LIKE '$name' AND ID<>'$id'")->row();
        }else{
            
            $names=$this->db->query("SELECT COUNT(ID) name_count FROM products_services WHERE NAME LIKE '$name'")->row();
        }
        
        if($names->name_count > 0){
            
            return FALSE;
        }
        
        return TRUE;
    }
    
    function products_services($id){
        
        if($id <> null){
            
            $where =" WHERE ID='$id'";
        }
        
        return $this->db->query("SELECT ID,NAME,DESCRIPTION,STATUS FROM products_services $where ORDER BY NAME ASC")->result();
    }
    
    function product_service_registration($prd_srv,$id){
        
         if($id == null){
          
          $prd_srv['ACTIONDATE']=date('Y-m-d H:i:s');
          return $this->db->insert('products_services',$prd_srv);
          
          
      } 
              $prd_srv['LASTUPDATE']=date('Y-m-d H:i:s');
              
        return  $this->db->update('products_services',$prd_srv,array('ID'=>$id));
    }
    
    function product_service_count($name){
        
        if($name <> null){
           $where =" WHERE NAME LIKE '%$name%'"; 
        }
        
        
        
        return count($this->db->query("SELECT ID,NAME,DESCRIPTION,STATUS "
                . "FROM products_services $where "
                . "ORDER BY NAME ASC")->result());
    }
    
    function product_service($name,$page,$limit){
        
        if($name <> null){
           $where =" WHERE NAME LIKE '%$name%'"; 
        }
        
        
        
        return $this->db->query("SELECT ID,NAME,DESCRIPTION,STATUS "
                . "FROM products_services $where "
                . "ORDER BY NAME ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function activate_deactivate_prd_srv($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->db->update('products_services',array('STATUS'=>$new_status),array('ID'=>$id));
    }
    
    function perfomance_durations($id){
        
        if($id <> null){
            $where=" WHERE ID='$id'";
        }
        
        return $this->db->query("SELECT ID,NAME,DURATION FROM perfomance_durations $where ORDER BY DURATION ASC")->result();
    }
    
    function get_users($id){
        
        if($id <> null){
            $where .=" AND users.ID='$id'";
        }
        
        return $this->db->query("SELECT users.ID,users.FIRST_NAME,"
                . "users.MIDDLE_NAME,users.LAST_NAME,users.EMAIL,"
                . "users.MSISDN,users.USERNAME,users.ACTIVE,users_groups.GROUP_ID "
                . "FROM users INNER JOIN users_groups "
                . "ON users.ID=users_groups.USER_ID WHERE users_groups.GROUP_ID=3 $where "
                . "ORDER BY users.FIRST_NAME ASC ")->result();
    }
    
    function activity_updates_info($user,$start,$end,$project,$begin,$limit){
        
        if($user <> null){
            
           $where .=" AND createdby='$user'"; 
        }
        
        if($start <> null){
            
           $where .=" AND createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND createdon <='$end'"; 
        }
        
        if($project <> null){
            
           $where .=" AND project LIKE '%$project%'"; 
        }
        
        return $this->db->query("SELECT id,project,comments,"
                . "status,projections,createdon,"
                . "createdby,pendings,activities,tasktitle,assigned,duedate,completiondate "
                . "FROM activity_updates WHERE id is not null $where "
                . "ORDER BY createdon DESC "
                . "LIMIT $begin,$limit")->result();
    }
    
    function activity_updates_count($user,$start,$end,$project){
        
        if($user <> null){
            
           $where .=" AND createdby='$user'"; 
        }
        
        if($start <> null){
            
           $where .=" AND createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND createdon <='$end'"; 
        }
        
        if($project <> null){
            
           $where .=" AND project LIKE '%$project%'"; 
        }
        
        return count($this->db->query("SELECT id,project,comments,"
                . "status,projections,createdon,"
                . "createdby,pendings,activities,tasktitle,assigned,duedate,completiondate "
                . "FROM activity_updates WHERE id is not null $where")->result());
    }
    
    function activity_updates($id,$user,$start,$end){
        
        if($id <> null){
            
           $where .=" AND id='$id'"; 
        }
        
        if($user <> null){
            
           $where .=" AND createdby='$user'"; 
        }
        
        if($start <> null){
            
           $where .=" AND createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND createdon <='$end'"; 
        }
        
        
        
        return $this->db->query("SELECT id,project,comments,"
                . "status,projections,createdon,"
                . "createdby,pendings,activities,tasktitle,assigned,duedate,completiondate "
                . "FROM activity_updates WHERE id is not null $where "
                . "ORDER BY createdon DESC")->result();
    }
    
    function save_activity($data){
        
        return $this->db->insert('activity_updates',$data);
    }
}