<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadministrator extends CI_Controller {

	function __construct(){
		parent::__construct();
                $this->load->model('Superadministrator_model');
                
                $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
                $this->output->set_header('Pragma: no-cache');
        }
    
	public function index($id)
	{
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                
                $this->data['title']="Home";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/dashboard';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function create_users($id=null)
	{
                $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
                if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'superadministrator/create_users'),
                                            array('title'=>'View Users','link'=>'superadministrator/view_users'),
                                        );
               
                $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
                $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
                $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
                $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
                $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
                $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
                $this->form_validation->set_rules('group','User Group','required|xss_clean');
                
                 if($id <> null){
                     
                    if($this->input->post('password') <> null){
                        
                      $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                      $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');   
                    }
                }else{

                  $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                  $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');  
                }
                
                
                
                 if ($this->form_validation->run() == TRUE)
                {
                     
                    
                     $firstname=$this->input->post('first_name');
                     $middlename=$this->input->post('middle_name');
                     $lastname=$this->input->post('last_name');
                     $username=$this->input->post('username');
                     $email=$this->input->post('email');
                     $mobile=$this->input->post('mobile');
                     $group=$this->input->post('group');
                     $password=$this->input->post('password');
                     
                    
                        
                            $user=array(
                                'FIRST_NAME'=>$firstname,
                                'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                                'IP_ADDRESS'=>$this->input->ip_address(),
                                'LAST_NAME'=>$lastname,
                                'USERNAME'=>$username,
                                'EMAIL'=>$email<>null?$email:null,
                                'MSISDN'=>$mobile,
                            );

                            $user_group=array(
                                'GROUP_ID'=>$group
                            );

                            //check username uniqueness
                            $username_check=$this->username_uniqueness($username,$id);
                            if($username_check){


                                //do registration
                                 $resp=$this->Superadministrator_model->system_user_registration($user,$user_group,$id,$password);

                                   if($resp){
                                       
                                       $this->data['message']="registration successfull!";
                                       $this->data['message_class']="form_success";
                                       redirect(current_url());
                                   }else{
                                       
                                       $this->data['message']="registration failed!";
                                       $this->data['message_class']="form_error";
                                   }
                            }else{

                                $this->data['message']="username exists already!";
                                $this->data['message_class']="form_error";
                            }
                  
                }
               
                $this->data['title']="Create Users";
                $this->data['id']="$id";
                $this->data['member']=$this->Superadministrator_model->get_member_info($id);
                $this->data['groups']=$this->Superadministrator_model->get_registration_groups();
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/create_users';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function view_users()
	{
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'superadministrator/create_users'),
                                            array('title'=>'View Users','link'=>'superadministrator/view_users'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('company')) {
                    $key['company'] = $this->input->post('company');
                    $this->data['company']=$this->input->post('company');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['company'] = $exp[3];
                    $this->data['company']=$key['company'];
                }
                
                $name =$key['name'];
                $company =$key['company'];
                
                $config["base_url"] = base_url() . "index.php/superadministrator/view_users/name_".$key['name']."_company_".$key['company']."/";
                $config["total_rows"] =$this->Superadministrator_model->member_info_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['members'] = $this->Superadministrator_model->member_info($name,$page,$limit);
                
                $this->data['title']="System Users";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/view_users';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function register_products_services($id=null)
	{
                $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
                if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'Register Product/Service','link'=>'superadministrator/register_products_services'),
                                            array('title'=>'View Product/Service','link'=>'superadministrator/view_products_services'),
                                        );
               
                $this->form_validation->set_rules('name','Name','required|xss_clean');
                $this->form_validation->set_rules('description','Description','xss_clean|required');
                
                
                
                
                 if ($this->form_validation->run() == TRUE)
                {
                     
                    
                     $name=$this->input->post('name');
                     $description=$this->input->post('description');
                     
                    
                        
                            $prod=array(
                                'NAME'=>$name,
                                'DESCRIPTION'=>trim(nl2br($description))
                                );


                            //check username uniqueness
                            $name_check=$this->product_service_uniqueness($name,$id);
                            
                            if($name_check){


                                //do registration
                                 $resp=$this->Superadministrator_model->product_service_registration($prod,$id);

                                   if($resp){
                                       
                                       $this->data['message']="registration successfull!";
                                       $this->data['message_class']="form_success";
                                       redirect(current_url());
                                   }else{
                                       
                                       $this->data['message']="registration failed!";
                                       $this->data['message_class']="form_error";
                                   }
                            }else{

                                $this->data['message']="product/service name already exists!";
                                $this->data['message_class']="form_error";
                            }
                  
                }
               
                $this->data['title']="Register Products/Services";
                $this->data['id']="$id";
                $this->data['prd_srv']=$this->Superadministrator_model->products_services($id);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/register_products_services';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function view_products_services()
	{
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'Register Product/Service','link'=>'superadministrator/register_products_services'),
                                            array('title'=>'View Product/Service','link'=>'superadministrator/view_products_services'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                }
                
                $name =$key['name'];
                $company =$key['company'];
                
                $config["base_url"] = base_url() . "index.php/superadministrator/view_products_services/name_".$key['name']."/";
                $config["total_rows"] =$this->Superadministrator_model->product_service_count($name);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['prd_srv'] = $this->Superadministrator_model->product_service($name,$page,$limit);
                
                $this->data['title']="Products/Services";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/view_products_services';
		$this->load->view('superadministrator/template',$this->data);
	}
        
		public function activity_updates(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'superadministrator/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'superadministrator/fill_activity_update'),
                                        );
                
                
                if ($this->input->post('start')) {
                    $key['start'] = $this->input->post('start');
                    $this->data['start']=$this->input->post('start');
                }
                
                if ($this->input->post('end')) {
                    $key['end'] = $this->input->post('end');
                    $this->data['end']=$this->input->post('end');
                }
                
                if ($this->input->post('user')) {
                    $key['user'] = $this->input->post('user');
                    $this->data['user']=$this->input->post('user');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['start'] = $exp[1];
                    $this->data['start']=$key['start'];
                    
                    $key['end'] = $exp[3];
                    $this->data['end']=$key['end'];
                    
                    $key['user'] = $exp[5];
                    $this->data['user']=$key['user'];
                }
                
                $start =$key['start'];
                $end =$key['end'];
                $user =$key['user'];
                
                $config["base_url"] = base_url() . "index.php/superadministrator/activity_updates/start_".$key['start']."_end_".$key['end']."_usr_".$key['user']."/";
                $config["total_rows"] =$this->Superadministrator_model->activity_updates_count($user,$start,$end);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['activity_updates'] = $this->Superadministrator_model->activity_updates_info($user,$start,$end,$page,$limit);
                
                $this->data['title']="Activity Updates";
                $this->data['users']=$this->Superadministrator_model->get_member_info();
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/activity_updates';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function activity_update_details($update_id){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'superadministrator/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'superadministrator/fill_activity_update'),
                                        );
                
                
                $this->data['activity_updates']=$this->Superadministrator_model->activity_updates($update_id);
                $this->data['title']="Activity Details";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/update_details';
		$this->load->view('superadministrator/template',$this->data);
	}
        
        public function fill_activity_update(){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'superadministrator/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'superadministrator/fill_activity_update'),
                                        );
                
                    $this->form_validation->set_rules('project','Project','required|xss_clean|required');
                    $this->form_validation->set_rules('status', 'Status', 'xss_clean|required');
                    $this->form_validation->set_rules('activities', 'Activities', 'xss_clean|required');
                    $this->form_validation->set_rules('pendings', 'Pending', 'xss_clean|required');
                    $this->form_validation->set_rules('projections', 'Projection', 'xss_clean|required');
                    $this->form_validation->set_rules('comments', 'Comments', 'xss_clean|required');


                    if($this->form_validation->run() == true){

                        $project=$this->input->post('project');
                        $status=$this->input->post('status');
                        $activities=trim(nl2br($this->input->post('activities')));
                        $pendings=trim(nl2br($this->input->post('pendings')));
                        $projections=trim(nl2br($this->input->post('projections')));
                        $comments=trim(nl2br($this->input->post('comments')));
                       
                       $array=array(
                                   'project'=>$project,
                                   'status'=>$status,
                                   'activities'=>$activities,
                                   'pendings'=>$pendings,
                                   'projections'=>$projections,
                                   'comments'=>$comments,
                                   'createdon'=>date('Y-m-d H:i:s'),
                                   'createdby'=>$this->session->userdata('user_id'),
                                );
                       
                            //add operational
                            $resp=$this->Superadministrator_model->save_activity($array);

                              if($resp){
                                 
                                  $this->data['message']="data saved!";
                                  $this->data['message_class']="form_success";
                              }else{

                                  $this->data['message']="registration failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                $this->data['title']="Fill Activity Update";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='superadministrator/fill_activity_update';
                $this->load->view('superadministrator/template',$this->data);
       }
	   
        public function activate_deactivate_users($id,$status){
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->Superadministrator_model->activate_deactivate_users($id,$status);
                redirect('superadministrator/view_users','refresh');
        }
        
        public function activate_deactivate_prd_srv($id,$status){
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->Superadministrator_model->activate_deactivate_prd_srv($id,$status);
                redirect('superadministrator/view_products_services','refresh');
        }
        
        public function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('SuperAdministrator')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
        
        public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'superadministrator/edit_profile'),
                                           array('title'=>'Change Password','link'=>'superadministrator/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='superadministrator/profile';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('superadministrator/template',$this->data);
        }
        
        public function edit_profile(){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'superadministrator/edit_profile'),
                                           array('title'=>'Change Password','link'=>'superadministrator/change_password'),
                                        );
                $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
                $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
                $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
                $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
                $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
                $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
                
                if($this->form_validation->run() == true){
                    
                     $firstname=$this->input->post('first_name');
                     $middlename=$this->input->post('middle_name');
                     $lastname=$this->input->post('last_name');
                     $username=$this->input->post('username');
                     $email=$this->input->post('email');
                     $mobile=$this->input->post('mobile');
                     
                    $user=array(
                                'FIRST_NAME'=>$firstname,
                                'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                                'LAST_NAME'=>$lastname,
                                'USERNAME'=>$username,
                                'EMAIL'=>$email<>null?$email:null,
                                'MSISDN'=>$mobile,
                            );
                    
                    $user_group=array(
                                'GROUP_ID'=>$this->session->userdata('group')
                            );
                    
                    $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                    if($username_check){


                        //do registration
                         $resp=$this->Superadministrator_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                           if($resp){

                               redirect('superadministrator/profile','refresh');
                           }else{

                               $this->data['message']="registration failed!";
                               $this->data['message_class']="form_error";
                           }
                    }else{

                        $this->data['message']="username exists already!";
                        $this->data['message_class']="form_error";
                    }
                }
                
                $this->data['title']="Edit Profile";
                $this->data['content']='superadministrator/edit_profile';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('superadministrator/template',$this->data);
        }
        
        public function change_password(){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'superadministrator/edit_profile'),
                                           array('title'=>'Change Password','link'=>'superadministrator/change_password'),
                                        );
                
                $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
                $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 
                
                if($this->form_validation->run() == true){
                    
                    $password=$this->input->post('password');
                    $pchange=$this->Superadministrator_model->change_password($password);
                    
                    if($pchange){

                               $this->data['message']="password change successfull!";
                               $this->data['message_class']="form_success";
                    }else{

                        $this->data['message']="password change failed!";
                        $this->data['message_class']="form_error";
                    }
                }
                
                $this->data['title']="Change Password";
                $this->data['content']='superadministrator/change_password';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('superadministrator/template',$this->data);
        }
        
        function username_uniqueness($username,$id){
            
            return $this->Superadministrator_model->check_username($username,$id);
        }
        
        function product_service_uniqueness($username,$id){
            
            return $this->Superadministrator_model->check_product_service_name($username,$id);
        }
        
        function cur_password($password){
            
            $pf=$this->Superadministrator_model->profile_data();
            $hashed_old=$this->Superadministrator_model->hash_password($password,$pf->SALT);
            
             if($pf->PASSWORD <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
        }
        
        function check_bundle_plan_uniqueness($data,$id){
            
            return $this->Superadministrator_model->check_bundle_plan_uniqueness($data,$id);
        }
        
        function stacked_bar_graph(){
           //date('Y-m-d', strtotime('-7 days'))
           $data=$this->Operator_model->retrieve_bar_graph_data();
            
             $params = array('SetTitle' => 'Bar Graph',// title 
                'SetDataType' => 'text-data', 
                'SetPlotType' => 'bars',// chart the type of area bars linepoints lines pie points squared stackedbars thinbarline 
                ); 
             $this->graph->init(array('width'=>700,'height'=>500));
             $this->graph->setdata($data,$params);
             //$this->graph->SetImageBorderType('plain');

             $status=$this->Operator_model->sms_status();
             $legend=array();
             foreach($status as $key=>$value){
                 $legend[]=$value->STATUS;
             }
             
             
             # Make a legend for the 3 data sets plotted:
             $this->graph->obj->SetLegend($legend);
             $this->graph->obj->SetLegendPosition(1, 0, 'plot', 1, 0, -5, 5);;
             
             # Turn off X tick labels and ticks because they don't apply here:
             $this->graph->obj->SetXTickLabelPos('none');
             $this->graph->obj->SetXTickPos('none');

              // Graph generation 
             $this->graph->draw(); 
             // Generate a graph of the acquired IMG tag 
             return $this->graph->getimg(); 

       }
       
       function stacked_line_graph(){
           //date('Y-m-d', strtotime('-7 days'))
           $data=$this->Operator_model->retrieve_bar_graph_data();
            
             $params = array('SetTitle' => 'Line Graph',// title 
                'SetDataType' => 'text-data', 
                'SetPlotType' => 'lines',// chart the type of area bars linepoints lines pie points squared stackedbars thinbarline 
                ); 
             $this->graph->init(array('width'=>700,'height'=>500));
             $this->graph->setdata($data,$params);
             //$this->graph->SetImageBorderType('plain');

             $status=$this->Operator_model->sms_status();
             $legend=array();
             foreach($status as $key=>$value){
                 $legend[]=$value->STATUS;
             }
             
             
             # Make a legend for the 3 data sets plotted:
             $this->graph->obj->SetLegend($legend);
             $this->graph->obj->SetLegendPosition(1, 0, 'plot', 1, 0, -5, 5);;
             
             # Turn off X tick labels and ticks because they don't apply here:
             $this->graph->obj->SetXTickLabelPos('none');
             $this->graph->obj->SetXTickPos('none');

              // Graph generation 
             $this->graph->draw(); 
             // Generate a graph of the acquired IMG tag 
             return $this->graph->getimg(); 

       }
       
       function start_date($date) {
        $CI = & get_instance();
        if ($date != "") {
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
        }
    }
    
       function end_date($date) {
        $CI = & get_instance();
        if ($date != "") {
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
        }
    }
    
}
