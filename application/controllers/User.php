<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	
	function __construct(){
		parent::__construct();
                
                $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
                $this->output->set_header('Pragma: no-cache');
        }
    
       public function index($id)
	{
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
//                $this->data['side_menu']=array(
//                                           array('title'=>'Projects Close%','link'=>'user/index/1'),
//                                           array('title'=>'Projects Stage','link'=>'user/index/2')
//                                        );
                
                $this->data['title']="Dashboard";
                //$this->data['graph']=$this->stacked_line_graph($id);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/dashboard';
		$this->load->view('user/template',$this->data);
	}
        
       public function create_projects($id){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Create Project','link'=>'user/create_projects'),
                                           array('title'=>'View Projects','link'=>'user/view_projects'),
                                           array('title'=>'Add Contacts','link'=>'user/add_contacts'),
                                           array('title'=>'Fill Risks/Competition','link'=>'user/add_operational_details'),
                                        );
                
                $this->form_validation->set_rules('oppo_name','Project Name','required|xss_clean');
                $this->form_validation->set_rules('category','Project Category','xss_clean|required');
                $this->form_validation->set_rules('org','Organization','xss_clean|required');
                $this->form_validation->set_rules('description','Description','required|xss_clean');
                $this->form_validation->set_rules('salesrep','Sales Representative','required|xss_clean');
                
                $this->form_validation->set_rules('contract_val','Total Value','required|xss_clean');
                $this->form_validation->set_rules('perfomance_time', 'Perfomance Duration', 'required|xss_clean');
                $this->form_validation->set_rules('year_one', 'Expected Year 1', 'required|xss_clean');
                $this->form_validation->set_rules('year_two', 'Expected Year 2', 'xss_clean');
                $this->form_validation->set_rules('year_three', 'Expected Year 3', 'xss_clean');
                $this->form_validation->set_rules('year_four', 'Expected Year 4', 'xss_clean');
                
                $prod_srvs=$this->input->post('product');
              
                
                if($this->form_validation->run() == true){
                    
                    if($prod_srvs <> null){
                        
                        $oppo_name=$this->input->post('oppo_name');
                        $category=$this->input->post('category');
                        $org=$this->input->post('org');
                        $description= trim(nl2br($this->input->post('description')));
                        $salesrep=$this->input->post('salesrep');
                        
                        $contract_val=$this->input->post('contract_val');
                        $perfomance_time=$this->input->post('perfomance_time');
                        $year_one=$this->input->post('year_one');
                        $year_two=$this->input->post('year_two')<>null?$this->input->post('year_two'):null;
                        $year_three=$this->input->post('year_three')<>null?$this->input->post('year_three'):null;
                        $year_four=$this->input->post('year_four')<>null?$this->input->post('year_four'):null;
                        
                       $project=array(
                                   'NAME'=>$oppo_name,
                                   'CATEGORY'=>$category,
                                   'ORGANIZATION'=>$org,
                                   'DESCRIPTION'=>$description,
                                   'TOTALVALUE'=>$contract_val,
                                   'SALESREP'=>$salesrep,
                                   'PERFOMANCEDURATION'=>$perfomance_time,
                                   'YEAR1'=>$year_one,
                                   'YEAR2'=>$year_two,
                                   'YEAR3'=>$year_three,
                                   'YEAR4'=>$year_four,
                                );
                       
                       
                            //do registration
                            $resp=$this->User_model->create_project($project,$id);

                              if($resp){
                                  
                                  $prd_srv_array=array();          
                                  $prd_srv_count=count($prod_srvs);
                                  
                                  foreach($prod_srvs as $key=>$value){
                                      
                                      $prd_srv_array[]=array(
                                          'PROJECTID'=>$resp,
                                          'PRODUCTID'=>$value,
                                      );
                                  }
                                  
                                  $this->User_model->add_project_products_services($prd_srv_array,$resp);
                                  redirect(current_url());
                              }else{

                                  $this->data['message']="registration failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }else{

                           $this->data['message']="Please check atleast one product/service!";
                           $this->data['message_class']="form_error";
                       }
                     
                }
                
                $this->data['title']="Create Project";
                $this->data['id']=$id;
                $this->data['perfomance_durations']=$this->Superadministrator_model->perfomance_durations();
                $this->data['bcategories']=$this->User_model->business_category();
                $this->data['prd_srv']=$this->Superadministrator_model->products_services();
                $this->data['users']=$this->Superadministrator_model->get_users(null);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['project']=$this->User_model->project_details($id);
                $this->data['project_products']=$this->User_model->project_products($id);
                $this->data['content']='user/create_project';
		$this->load->view('user/template',$this->data);
        }
       
       public function view_projects(){
           if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Create Project','link'=>'user/create_projects'),
                                           array('title'=>'View Projects','link'=>'user/view_projects'),
                                           array('title'=>'Add Contacts','link'=>'user/add_contacts'),
                                           array('title'=>'Fill Risks/Competition','link'=>'user/add_operational_details'),
                                        );
           
           if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('category')) {
                    $key['category'] = $this->input->post('category');
                    $this->data['category']=$this->input->post('category');
                }
                
                if ($this->input->post('org')) {
                    $key['org'] = $this->input->post('org');
                    $this->data['org']=$this->input->post('org');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['org'] = $exp[3];
                    $this->data['org']=$key['org'];
                }
                
                $name =$key['name'];
                $org =$key['org'];
                $category =$key['category'];
                
                $config["base_url"] = base_url() . "index.php/user/view_projects/name_".$key['name']."_org_".$key['org']."_cat_".$key['category']."/";
                $config["total_rows"] =$this->User_model->projects_info_count($name,$org,$category);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['projects'] = $this->User_model->projects_info($name,$org,$category,$page,$limit);
                
                $this->data['title']="Projects";
                $this->data['content']='user/view_projects';
                $this->data['bcategories']=$this->User_model->business_category();
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
		$this->load->view('user/template',$this->data);
       }
        
       public function project_details($id){
           if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Create Project','link'=>'user/create_projects'),
                                           array('title'=>'View Projects','link'=>'user/view_projects'),
                                           array('title'=>'Add Contacts','link'=>'user/add_contacts'),
                                           array('title'=>'Fill Risks/Competition','link'=>'user/add_operational_details'),
                                        );
                
                $this->data['title']="Project Details";
                $this->data['id']=$id;
                $this->data['prd_srv']=$this->User_model->project_products_services(null,$id);
                $this->data['contacts']=$this->User_model->project_contacts(null,$id);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['project']=$this->User_model->project_details($id);
                $this->data['ops']=$this->User_model->operational_details(null,$id);
                $this->data['content']='user/project_details';
		$this->load->view('user/template',$this->data);
       }
       
       public function add_contacts($id){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Create Project','link'=>'user/create_projects'),
                                           array('title'=>'View Projects','link'=>'user/view_projects'),
                                           array('title'=>'Add Contacts','link'=>'user/add_contacts'),
                                           array('title'=>'Fill Risks/Competition','link'=>'user/add_operational_details'),
                                        );
                
                $this->form_validation->set_rules('project','Project','required|xss_clean');
                $this->form_validation->set_rules('name','Person','required|xss_clean');
                $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
                $this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
                $this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
                
                if($this->form_validation->run() == true){
                    
                        $project=$this->input->post('project');
                        $name=$this->input->post('name');
                        $email=$this->input->post('email');
                        $phone=$this->input->post('phone');
                        $title=$this->input->post('title');
                        
                        
                       $array=array(
                                   'PROJECTID'=>$project,
                                   'NAME'=>$name,
                                   'EMAIL'=>$email,
                                   'PHONE'=>$phone,
                                   'TITLE'=>$title,
                           );
                       
                           //add contact
                            $resp=$this->User_model->add_contact($array,$id);

                              if($resp){
                                  
                                  redirect(current_url());
                              }else{

                                  $this->data['message']="action failed!";
                                  $this->data['message_class']="form_error";
                              }
                }
                
                $this->data['title']="Add/Edit Contacts";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['project_contacts']=$this->User_model->project_contacts($id,null);
                $this->data['projects']=$this->User_model->projects();
                $this->data['id']=$id;
                $this->data['content']='user/add_contacts';
		$this->load->view('user/template',$this->data);
        }
        
       public function add_operational_details(){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Create Project','link'=>'user/create_projects'),
                                           array('title'=>'View Projects','link'=>'user/view_projects'),
                                           array('title'=>'Add Contacts','link'=>'user/add_contacts'),
                                           array('title'=>'Fill Risks/Competition','link'=>'user/add_operational_details'),
                                        );
                
                $this->form_validation->set_rules('project','Project','required|xss_clean');
                $this->form_validation->set_rules('risks', 'Risks', 'xss_clean');
                $this->form_validation->set_rules('competition', 'Competition', 'xss_clean');
                
                
                if($this->form_validation->run() == true){
                     
                        $project=$this->input->post('project');
                        $risks=trim(nl2br($this->input->post('risks')));
                        $competition=trim(nl2br($this->input->post('competition')));
                       
                       $array=array(
                                   'PROJECTID'=>$project,
                                   'RISKS'=>$risks,
                                   'COMPETITION'=>$competition,
                                   'ACTIONDATE'=>date('Y-m-d H:i:s'),
                                   'USERID'=>$this->session->userdata('user_id')
                                );
                       
                            //add operational
                            $resp=$this->User_model->add_operational_detail($array);

                              if($resp){

                                  redirect(current_url());
                              }else{

                                  $this->data['message']="registration failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                
                $this->data['title']="Add Operational Detail";
                $this->data['projects']=$this->User_model->projects();
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/add_operational';
                $this->load->view('user/template',$this->data);
        }
        
       public function fill_activities(){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Fill','link'=>'user/fill_activities'),
                                           array('title'=>'View','link'=>'user/view_activities'),
                                       );
                
                    $this->form_validation->set_rules('project','Project','required|xss_clean');
                    $this->form_validation->set_rules('close', 'Close %', 'xss_clean|required');
                    $this->form_validation->set_rules('stage', 'Stage', 'xss_clean|required');
                    $this->form_validation->set_rules('comments', 'Comments', 'xss_clean');


                    if($this->form_validation->run() == true){

                        $project=$this->input->post('project');
                        $close=$this->input->post('close');
                        $stage=$this->input->post('stage');
                        $comments=trim(nl2br($this->input->post('comments')));
                       
                       $array=array(
                                   'PROJECTID'=>$project,
                                   'CLOSE'=>$close,
                                   'STAGE'=>$stage,
                                   'COMMENTS'=>$comments,
                                   'USERID'=>$this->session->userdata('user_id')
                                );
                       
                            //add operational
                            $resp=$this->User_model->save_activity($array);

                              if($resp){
                                  
                                  if($stage == 4 && $close == 100){
                                      
                                       $this->User_model->close_project($project);
                                  }
                                  
                                  redirect(current_url());
                              }else{

                                  $this->data['message']="registration failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                $this->data['title']="Fill Activities";
                $this->data['projects']=$this->User_model->projects_user_specific($this->session->userdata('user_id'));
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/fill_activities';
                $this->load->view('user/template',$this->data);
       }
       
       public function view_activities(){
           if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Fill','link'=>'user/fill_activities'),
                                           array('title'=>'View','link'=>'user/view_activities'),
                                       );
           
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('org')) {
                    $key['org'] = $this->input->post('org');
                    $this->data['org']=$this->input->post('org');
                }

                if ($this->input->post('close')) {
                    $key['close'] = $this->input->post('close');
                    $this->data['close']=$this->input->post('close');
                }
                
                if ($this->input->post('stage')) {
                    $key['stage'] = $this->input->post('stage');
                    $this->data['stage']=$this->input->post('stage');
                }
                
                if ($this->input->post('start')) {
                    $key['start'] = $this->input->post('start');
                    $this->data['start']=$this->input->post('start');
                }
                
                if ($this->input->post('end')) {
                    $key['end'] = $this->input->post('end');
                    $this->data['end']=$this->input->post('end');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['org'] = $exp[3];
                    $this->data['org']=$key['org'];
                    
                    $key['close'] = $exp[5];
                    $this->data['close']=$key['close'];
                    
                    $key['stage'] = $exp[7];
                    $this->data['stage']=$key['stage'];
                    
                    $key['start'] = $exp[9];
                    $this->data['start']=$key['start'];
                    
                    $key['end'] = $exp[11];
                    $this->data['end']=$key['end'];
                    
                }
                
                $name =$key['name'];
                $org =$key['org'];
                $close =$key['close'];
                $stage =$key['stage'];
                $start =$key['start'];
                $end =$key['end'];
                
                $config["base_url"] = base_url() . "index.php/user/view_activities/name_".$key['name']."_org_".$key['org']."_close_".$key['close']."_stage_".$key['stage']."_start_".$key['start']."_end_".$key['end']."/";
                $config["total_rows"] =$this->User_model->activities_info_count($name,$org,$close,$stage,$start,$end);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['sales_activity'] = $this->User_model->activities_info($name,$org,$close,$stage,$start,$end,$page,$limit);
                
                $this->data['title']="View Activities";
                $this->data['content']='user/view_activities';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
		$this->load->view('user/template',$this->data);
       }
       
       public function fill_address($id){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Fill','link'=>'user/fill_address'),
                                           array('title'=>'View','link'=>'user/view_address'),
                                       );
                
                    $this->form_validation->set_rules('org','Organisation','required|xss_clean');
                    $this->form_validation->set_rules('phone1', 'Phone 1', 'xss_clean|required');
                    $this->form_validation->set_rules('phone2', 'Phone 2', 'xss_clean');
                    $this->form_validation->set_rules('phone3', 'Phone 3', 'xss_clean');
                    $this->form_validation->set_rules('email', 'Email', 'xss_clean|valid_email');
                    $this->form_validation->set_rules('name', 'Name', 'xss_clean|required');
                    $this->form_validation->set_rules('physical_address', 'Physical Address', 'xss_clean');
                    $this->form_validation->set_rules('comments', 'Comments', 'xss_clean');


                    if($this->form_validation->run() == true){

                        $org=$this->input->post('org');
                        $phone1=$this->input->post('phone1');
                        $phone2=$this->input->post('phone2');
                        $phone3=$this->input->post('phone3');
                        $email=$this->input->post('email');
                        $name=$this->input->post('name');
                        $phyaddress=$this->input->post('physical_address');
                        $comments=trim(nl2br($this->input->post('comments')));
                       
                       $array=array(
                                   'ORGANIZATION'=>$org,
                                   'PHONE1'=>$phone1,
                                   'PHONE2'=>$phone2<> null?$phone2:null,
                                   'PHONE3'=>$phone3<>null?$phone3:null,
                                   'NAME'=>$name,
                                   'EMAIL'=>$email<> null?$email:null,
                                   'PHY_ADDRESS'=>$phyaddress<> null?$phyaddress:null,
                                   'COMMENTS'=>$comments<>null?$comments:null,
                                );
                       
                            //add operational
                            $resp=$this->User_model->save_address($array,$id);

                              if($resp){
                                  
                                  redirect(current_url());
                              }else{

                                  $this->data['message']="saving failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                    
                $this->data['title']="Fill Address";
                $this->data['id']=$id;
                $this->data['address']=$this->User_model->address($id);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/fill_address';
                $this->load->view('user/template',$this->data);
       }
       
       public function view_business_categories(){
           if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Add','link'=>'user/add_business_categories'),
                                           array('title'=>'View','link'=>'user/view_business_categories')
                                        );
           
                if ($this->input->post('category')) {
                    $key['category'] = $this->input->post('category');
                    $this->data['category']=$this->input->post('category');
                }
                
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['category'] = $exp[1];
                    $this->data['category']=$key['category'];
                    
                   
                }
                
                $category =$key['category'];
                
                $config["base_url"] = base_url() . "index.php/user/view_business_categories/category_".$key['category']."/";
                $config["total_rows"] =$this->User_model->business_categories_count($category);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['bcategories'] = $this->User_model->business_categories($category,$page,$limit);
                
                $this->data['title']="View Business Categories";
                $this->data['content']='user/view_business_categories';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
		$this->load->view('user/template',$this->data);
       }
       
       public function add_business_categories($id){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                 $this->data['side_menu']=array(
                                           array('title'=>'Add','link'=>'user/add_business_categories'),
                                           array('title'=>'View','link'=>'user/view_business_categories')
                                        );
                
                    $this->form_validation->set_rules('category','Category','required|xss_clean');
                    $this->form_validation->set_rules('description', 'Description', 'xss_clean');


                    if($this->form_validation->run() == true){

                        $category=$this->input->post('category');
                        $description=trim(nl2br($this->input->post('description')));
                    
                       $array=array(
                                   'CATEGORY'=>$category,
                                   'DESCRIPTION'=>$description<>null?$description:null
                                );
                       
                       if($id == null){
                           
                           $array['CREATEDBY']=$this->session->userdata('user_id');
                           
                       }else{
                           $array['MODIFIEDBY']=$this->session->userdata('user_id');
                           $array['LASTUPDATE']=date('Y-m-d H:i:s');
                       }
                       
                            //add operational
                            $resp=$this->User_model->save_business_category($array,$id);

                              if($resp){
                                  
                                  redirect(current_url());
                              }else{

                                  $this->data['message']="saving failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                    
                $this->data['title']="Add Business Category";
                $this->data['id']=$id;
                $this->data['bcategory']=$this->User_model->business_category($id);
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/add_bcategory';
                $this->load->view('user/template',$this->data);
       }
       
       public function view_address(){
           if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Fill','link'=>'user/fill_address'),
                                           array('title'=>'View','link'=>'user/view_address'),
                                       );
           
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('org')) {
                    $key['org'] = $this->input->post('org');
                    $this->data['org']=$this->input->post('org');
                }

                if ($this->input->post('phone')) {
                    $key['phone'] = $this->input->post('phone');
                    $this->data['phone']=$this->input->post('phone');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['org'] = $exp[3];
                    $this->data['org']=$key['org'];
                    
                    $key['phone'] = $exp[5];
                    $this->data['phone']=$key['phone'];
                                       
                }
                
                $name =$key['name'];
                $org =$key['org'];
                $phone =$key['phone'];
                
                $config["base_url"] = base_url() . "index.php/user/view_address/name_".$key['name']."_org_".$key['org']."_phone_".$key['phone']."/";
                $config["total_rows"] =$this->User_model->address_info_count($name,$org,$phone);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['address'] = $this->User_model->address_info($name,$org,$phone,$page,$limit);
                
                $this->data['title']="View Address Book";
                $this->data['content']='user/view_address';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
		$this->load->view('user/template',$this->data);
       }
       
       function remove_address($id){
           
           $this->User_model->remove_address($id);
           
           redirect('user/view_address','refresh');
       }
       
       public function activity_updates(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'user/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'user/fill_activity_update'),
                                        );
                
                
                if ($this->input->post('start')) {
                    $key['start'] = $this->input->post('start');
                    $this->data['start']=$this->input->post('start');
                }
                
                if ($this->input->post('end')) {
                    $key['end'] = $this->input->post('end');
                    $this->data['end']=$this->input->post('end');
                }
                
                if ($this->input->post('user')) {
                    $key['user'] = $this->input->post('user');
                    $this->data['user']=$this->input->post('user');
                }
                
                if ($this->input->post('project')) {
                    $key['project'] = $this->input->post('project');
                    $this->data['project']=$this->input->post('project');
                }
                
               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['start'] = $exp[1];
                    $this->data['start']=$key['start'];
                    
                    $key['end'] = $exp[3];
                    $this->data['end']=$key['end'];
                    
                    $key['user'] = $exp[5];
                    $this->data['user']=$key['user'];
                    
                    $key['project'] = $exp[7];
                    $this->data['project']=$key['project'];
                }
                
                $start =$key['start'];
                $end =$key['end'];
                $user =$key['user'];
                $project =$key['project'];
                
                $config["base_url"] = base_url() . "index.php/user/activity_updates/start_".$key['start']."_end_".$key['end']."_usr_".$key['user']."_prj_".$key['project']."/";
                $config["total_rows"] =$this->Superadministrator_model->activity_updates_count($user,$start,$end,$project);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['activity_updates'] = $this->Superadministrator_model->activity_updates_info($user,$start,$end,$project,$page,$limit);
                
                $this->data['title']="Activity Updates";
                $this->data['users']=$this->Superadministrator_model->get_member_info();
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/activity_updates';
		$this->load->view('user/template',$this->data);
	}
        
       public function activity_update_details($update_id){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'user/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'user/fill_activity_update'),
                                        );
                
                
                $this->data['activity_updates']=$this->Superadministrator_model->activity_updates($update_id);
                $this->data['title']="Activity Details";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/update_details';
		$this->load->view('user/template',$this->data);
	}
        
       public function fill_activity_update(){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                            array('title'=>'View Updates','link'=>'user/activity_updates'),
                                            array('title'=>'Fill Updates','link'=>'user/fill_activity_update'),
                                        );
                
                    $this->form_validation->set_rules('project','Project','required|xss_clean|required');
                    $this->form_validation->set_rules('status', 'Status', 'xss_clean|required');
                    $this->form_validation->set_rules('activities', 'Activities', 'xss_clean|required');
                    $this->form_validation->set_rules('pendings', 'Pending', 'xss_clean|required');
                    $this->form_validation->set_rules('projections', 'Projection', 'xss_clean|required');
                    $this->form_validation->set_rules('comments', 'Comments', 'xss_clean|required');
                    $this->form_validation->set_rules('tasktitle', 'Task Title', 'xss_clean|required');
                    $this->form_validation->set_rules('assigned', 'Assigned To', 'xss_clean|required');
                    $this->form_validation->set_rules('duedate', 'Due Date', 'xss_clean|required');
                    $this->form_validation->set_rules('completiondate', 'Completion Date', 'xss_clean|required');


                    if($this->form_validation->run() == true){

                        $project=$this->input->post('project');
                        $status=$this->input->post('status');
                        $activities=trim(nl2br($this->input->post('activities')));
                        $pendings=trim(nl2br($this->input->post('pendings')));
                        $projections=trim(nl2br($this->input->post('projections')));
                        $comments=trim(nl2br($this->input->post('comments')));
                        $tasktitle=trim(nl2br($this->input->post('tasktitle')));
                        $assigned=trim(nl2br($this->input->post('assigned')));
                        $duedate=trim(nl2br($this->input->post('duedate')));
                        $completiondate=trim(nl2br($this->input->post('completiondate')));
                       
                       $array=array(
                                   'project'=>$project,
                                   'status'=>$status,
                                   'activities'=>$activities,
                                   'pendings'=>$pendings,
                                   'projections'=>$projections,
                                   'comments'=>$comments,
                                   'createdon'=>date('Y-m-d H:i:s'),
                                   'createdby'=>$this->session->userdata('user_id'),
                                   'tasktitle'=>$tasktitle,
                                   'assigned'=>$assigned,
                                   'duedate'=>$duedate,
                                   'completiondate'=>$completiondate,
                                );
                       
                            //add operational
                            $resp=$this->Superadministrator_model->save_activity($array);

                              if($resp){
                                 
                                  $this->data['message']="data saved!";
                                  $this->data['message_class']="form_success";
                              }else{

                                  $this->data['message']="registration failed!";
                                  $this->data['message_class']="form_error";
                              }
                    }
                $this->data['title']="Fill Activity Update";
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['content']='user/fill_activity_update';
                $this->load->view('user/template',$this->data);
       }
       
       public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'user/edit_profile'),
                                           array('title'=>'Change Password','link'=>'user/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='user/profile';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('user/template',$this->data);
        }
        
       public function edit_profile(){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'user/edit_profile'),
                                           array('title'=>'Change Password','link'=>'user/change_password'),
                                        );
                $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
                $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
                $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
                $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
                $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
                $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
                
                if($this->form_validation->run() == true){
                    
                     $firstname=$this->input->post('first_name');
                     $middlename=$this->input->post('middle_name');
                     $lastname=$this->input->post('last_name');
                     $username=$this->input->post('username');
                     $email=$this->input->post('email');
                     $mobile=$this->input->post('mobile');
                     
                    $user=array(
                                'FIRST_NAME'=>$firstname,
                                'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                                'LAST_NAME'=>$lastname,
                                'USERNAME'=>$username,
                                'EMAIL'=>$email<>null?$email:null,
                                'MSISDN'=>$mobile,
                            );
                    
                    $user_group=array(
                                'GROUP_ID'=>$this->session->userdata('group')
                            );
                    
                    $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                    if($username_check){


                        //do registration
                         $resp=$this->Superadministrator_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                           if($resp){

                               redirect('user/profile','refresh');
                           }else{

                               $this->data['message']="registration failed!";
                               $this->data['message_class']="form_error";
                           }
                    }else{

                        $this->data['message']="username exists already!";
                        $this->data['message_class']="form_error";
                    }
                }
                
                $this->data['title']="Edit Profile";
                $this->data['content']='user/edit_profile';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('user/template',$this->data);
        }
        
       public function change_password(){
             $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'user/edit_profile'),
                                           array('title'=>'Change Password','link'=>'user/change_password'),
                                        );
                
                $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
                $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 
                
                if($this->form_validation->run() == true){
                    
                    $password=$this->input->post('password');
                    $pchange=$this->Superadministrator_model->change_password($password);
                    
                    if($pchange){

                               $this->data['message']="password change successfull!";
                               $this->data['message_class']="form_success";
                    }else{

                        $this->data['message']="password change failed!";
                        $this->data['message_class']="form_error";
                    }
                }
                
                $this->data['title']="Change Password";
                $this->data['content']='user/change_password';
                $this->data['userInfo']=$this->Superadministrator_model->current_user_info();
                $this->data['profile']=$this->Superadministrator_model->profile_data();
		$this->load->view('user/template',$this->data);
        }
        
       public function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('User')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
        
       function username_uniqueness($username,$id){
            
            return $this->Superadministrator_model->check_username($username,$id);
        }
        
       function cur_password($password){
            
            $pf=$this->Superadministrator_model->profile_data();
            $hashed_old=$this->Superadministrator_model->hash_password($password,$pf->SALT);
            
             if($pf->PASSWORD <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
        }
        
       function stacked_line_graph($id){
           //date('Y-m-d', strtotime('-7 days'))
           $data=$this->User_model->retrieve_graph_data($id);
           
           if($id == 1){
               $title='Close% vs Projects';
           }else{
               $title='Stage vs Projects';
           }
           
             $params = array('SetTitle' =>$title,// title 
                'SetDataType' => 'text-data', 
                'SetPlotType' => 'lines',// chart the type of area bars linepoints lines pie points squared stackedbars thinbarline 
                ); 
             $this->graph->init(array('width'=>700,'height'=>500));
             $this->graph->setdata($data,$params);
             //$this->graph->SetImageBorderType('plain');

             $pjcts=$this->User_model->distinct_projects();
             $legend=array();
             foreach($pjcts as $key=>$value){
                 
                 $legend[]=$value->NAME;
             }
             
             
             # Make a legend for the 3 data sets plotted:
             $this->graph->obj->SetLegend($legend);
             $this->graph->obj->SetLegendPosition(1, 0, 'plot', 1, 0, -5, 5);;
             
             # Turn off X tick labels and ticks because they don't apply here:
             $this->graph->obj->SetXTickLabelPos('none');
             $this->graph->obj->SetXTickPos('none');

              // Graph generation 
             $this->graph->draw(); 
             // Generate a graph of the acquired IMG tag 
             return $this->graph->getimg(); 

       }
       
       function start_date($date) {
        $CI = & get_instance();
        if ($date != "") {
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
        }
    }
    
       function end_date($date) {
        $CI = & get_instance();
        if ($date != "") {
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $CI->form_validation->set_message('valid_date', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
        }
    }
    
       function remove_risk($id,$projectid){
           
           $this->User_model->remove_risk($id);
           redirect('user/project_details/'.$projectid, 'refresh');
       }
       
       function remove_competition($id,$projectid){
           
           $this->User_model->remove_competition($id);
           redirect('user/project_details/'.$projectid, 'refresh');
       }
}