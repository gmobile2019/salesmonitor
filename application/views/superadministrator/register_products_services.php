
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('superadministrator/register_products_services/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="name" id="name" placeholder="Name" value="<?php echo $id != null?$prd_srv[0]->NAME:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="description" id="description">
                            <?php echo $id != null?$prd_srv[0]->DESCRIPTION:set_value('description'); ?>
                        </textarea>
                           <?php echo form_error('description'); ?>
                    </div>
                </div>
               <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Register Product/Service</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
