
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('superadministrator/edit_profile/'); 
                ?>
       
                <div class="form-group row">
                    <label for="first_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">First Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="first_name" id="first_name" placeholder="First Name"  value="<?php echo set_value('first_name',$profile->FIRST_NAME); ?>" />
                        <?php echo form_error('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middle_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Middle Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="middle_name" id="middle_name" placeholder="Middle Name"  value="<?php echo set_value('middle_name',$profile->MIDDLE_NAME); ?>" />
                        <?php echo form_error('middle_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Last Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="last_name" id="last_name" placeholder="Last Name"  value="<?php echo set_value('last_name',$profile->LAST_NAME); ?>" />
                        <?php echo form_error('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Username&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="username" id="username" placeholder="Username"  value="<?php echo set_value('username',$profile->USERNAME); ?>" />
                        <?php echo form_error('username'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="email" id="email" placeholder="Email"  value="<?php echo set_value('email',$profile->EMAIL); ?>" />
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Mobile&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control  edit" name="mobile" id="mobile" placeholder="Mobile Number"  value="<?php echo set_value('mobile',$profile->MSISDN); ?>" />
                        <?php echo form_error('mobile'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
