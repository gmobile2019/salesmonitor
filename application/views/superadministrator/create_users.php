
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('superadministrator/create_users/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="first_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">First Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="first_name" id="first_name" placeholder="First Name" value="<?php echo $id != null?$member[0]->FIRST_NAME:set_value('first_name'); ?>" />
                        <?php echo form_error('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middle_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Middle Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="middle_name" id="middle_name" placeholder="Middle Name" value="<?php echo $id != null?$member[0]->MIDDLE_NAME:set_value('middle_name'); ?>"/>
                        
                            <?php echo form_error('middle_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Last Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $id != null?$member[0]->LAST_NAME:set_value('last_name'); ?>" />
                        <?php echo form_error('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Username&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="username" id="username" placeholder="Username"  value="<?php echo $id != null?$member[0]->USERNAME:set_value('username'); ?>" />
                        <?php echo form_error('username'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="email" id="email" placeholder="Email" value="<?php echo $id != null?$member[0]->EMAIL:set_value('email'); ?>"/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Mobile&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $id != null?$member[0]->MSISDN:set_value('mobile'); ?>"/>
                        <?php echo form_error('mobile'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="password" id="password" placeholder="Password"/>
                        <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="conf_password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Confirm Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="conf_password" id="conf_password" placeholder="Confirm Password"/>
                        <?php echo form_error('conf_password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="group" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">User Group&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="group" id="group" class="form-control " >
                        <option></option>
                        <?php foreach($groups as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($member[0]->GROUP_ID))?'selected="selected"':set_select('group',$value->ID); ?>><?php echo $value->NAME; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('group'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Register User</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
