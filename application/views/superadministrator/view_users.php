<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('superadministrator/view_users',$attributes); 
                ?>
      <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label class="sr-only" for="name"></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="First/Middle/Last" value="<?php echo $name; ?>" />
      </div>
     <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <button type="submit" class="btn btn-success">Search</button>
      </div>
        
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Full name</th>
                <th style="text-align:center;width:200px">Category</th>
                <th style="text-align:center;width:300px">Mobile</th>
                <th style="text-align:center;width:300px">Email</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($members != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($members as $key=>$value){
                    $category=$this->Superadministrator_model->groups($value->GROUP_ID);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->FIRST_NAME.' '.$value->MIDDLE_NAME.' '.$value->LAST_NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $category[0]->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->MSISDN; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->EMAIL; ?></td>
                        <?php 
                        
                        $active_status=$value->ACTIVE == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('superadministrator/create_users/'.$value->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('superadministrator/activate_deactivate_users/'.$value->ID.'/'.$value->ACTIVE,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
                <tr>
                    <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>
    
</div>
