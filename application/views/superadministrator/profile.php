<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th  colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
               
                    $category=$this->Superadministrator_model->groups($profile->GROUP_ID);
                    ?>
                    <tr>
                        <td>First Name</td>
                        <td>&nbsp;&nbsp;<?php echo $profile->FIRST_NAME; ?></td>
                    </tr>
                    <tr>
                       <td>Middle Name</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->MIDDLE_NAME; ?></td> 
                    </tr>
                    <tr>
                        <td>Last Name</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->LAST_NAME; ?>
                           
                       </td> 
                    </tr>
                    <tr>
                        <td>Username</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->USERNAME; ?></td> 
                    </tr>
                    <tr>
                        <td>User Group</td>
                        <td>&nbsp;&nbsp;<?php echo $category[0]->NAME; ?></td>
                    </tr>
                    <tr>
                       <td>Mobile Number</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->MSISDN; ?>
                       </td> 
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>&nbsp;&nbsp;<?php echo $profile->EMAIL; ?></td>  
                    </tr>
            </tbody>
        </table>
    </div>
    
</div>
