
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('superadministrator/view_products_services',$attributes); 
                ?>
      <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-6">
            <label class="sr-only" for="name"></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
      </div>
     <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <button type="submit" class="btn btn-success">Search</button>
      </div>
        
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px">
    <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Name</th>
                <th style="text-align:center;width:550px">Description</th>
                <th style="text-align:center;width:300px">Status</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($prd_srv != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($prd_srv as $key=>$value){
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->DESCRIPTION; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->STATUS ==1?'Active':'Inactive'; ?></td>
                        <?php 
                        
                        $active_status=$value->STATUS == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('superadministrator/register_products_services/'.$value->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('superadministrator/activate_deactivate_prd_srv/'.$value->ID.'/'.$value->STATUS,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
