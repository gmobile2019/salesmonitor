
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/fill_address/'.$id); 
                ?>
                <div class="form-group row">
                            <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="name" id="name" placeholder="Name" value="<?php echo $id != null?$address[0]->NAME:set_value('name'); ?>"/>

                                <?php echo form_error('name'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="org" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Organization&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="org" id="org" placeholder="Organization" value="<?php echo $id != null?$address[0]->ORGANIZATION:set_value('org'); ?>"/>

                                <?php echo form_error('org'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="phone1" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone 1&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="phone1" id="phone1" placeholder="Phone 1" value="<?php echo $id != null?$address[0]->PHONE1:set_value('phone1'); ?>"/>

                                <?php echo form_error('phone1'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="phone2" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone 2</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="phone2" id="phone2" placeholder="Phone 2" value="<?php echo $id != null?$address[0]->PHONE2:set_value('phone2'); ?>"/>

                                <?php echo form_error('phone2'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="phone3" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone 3</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="phone3" id="phone3" placeholder="Phone 3" value="<?php echo $id != null?$address[0]->PHONE3:set_value('phone3'); ?>"/>

                                <?php echo form_error('phone3'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="email" id="email" placeholder="Email" value="<?php echo $id != null?$address[0]->EMAIL:set_value('email'); ?>"/>

                                <?php echo form_error('email'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="physical_address" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Physical Address</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <input type="text" class="form-control " name="physical_address" id="physical_address" placeholder="Physical Address" value="<?php echo $id != null?$address[0]->PHY_ADDRESS:set_value('physical_address'); ?>"/>

                                <?php echo form_error('physical_address'); ?>
                            </div>
                </div>
                <div class="form-group row">
                    <label for="comments" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Comments&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="comments" id="comments"><?php echo set_value('comments'); ?></textarea>
                        <?php echo form_error('comments'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
