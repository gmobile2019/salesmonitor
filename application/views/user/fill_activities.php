
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/fill_activities/'); 
                ?>
                <div class="form-group row">
                            <label for="project" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Project&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <select name="project" id="project" class="form-control " >
                                <option></option>
                                <?php foreach($projects as $key=>$value){ ?>

                                <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project_contacts[0]->PROJECTID))?'selected="selected"':set_select('project',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                    <?php } ?>

                            </select>
                                <?php echo form_error('project'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="close" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Close %&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <select name="close" id="close" class="form-control " >
                                <option value=""></option>
                                <option value="20">20%</option>
                                <option value="40">40%</option>
                                <option value="60">60%</option>
                                <option value="80">80%</option>
                                <option value="100">100%</option>
                            </select>
                                <?php echo form_error('close'); ?>
                            </div>
                </div>
                <div class="form-group row">
                            <label for="stage" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Stage&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <select name="stage" id="stage" class="form-control " >
                                <option  value=""></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                                <?php echo form_error('stage'); ?>
                            </div>
                </div>
                <div class="form-group row">
                    <label for="comments" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Comments&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="comments" id="comments"><?php echo set_value('comments'); ?></textarea>
                        <?php echo form_error('comments'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Activity</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
