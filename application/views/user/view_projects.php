<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('user/view_projects',$attributes); 
                ?>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <label class="sr-only" for="name"></label>
                <input type="text" class="form-control " name="name" id="name" placeholder="Project Name" value="<?php echo $name; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <label class="sr-only" for="org"></label>
            <input type="text" class="form-control " name="org" id="org" placeholder="Organization" value="<?php echo $org; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <label class="sr-only" for="category"></label>
            <select name="category" id="category" class="form-control " >
                    <option value=""></option>
                    <?php foreach($bcategories as $key=>$value){ ?>
                    
                    <option value="<?php echo $value->ID; ?>"><?php echo $value->CATEGORY; ?></option>
                    
                    <?php } ?>
            </select>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
           <button type="submit" class="btn btn-success">Search</button>
    </div>
        
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Name</th>
                <th style="text-align:center;width:250">Category</th>
                <th style="text-align:center;width:200px">Organization</th>
                <th style="text-align:center;width:300px">Total Value</th>
                <th style="text-align:center;width:300px">Duration</th>
                <th style="text-align:center;width:300px">Status</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($projects != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($projects as $key=>$value){
                    $duration=$this->User_model->get_duration($value->PERFOMANCEDURATION);
                    $bcateg=$this->User_model->business_category($value->CATEGORY);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $bcateg[0]->CATEGORY; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->ORGANIZATION; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->TOTALVALUE; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $duration[0]->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->STATUS == 0?'Open':'Closed'; ?></td>
                        <td style="text-align: center">&nbsp;&nbsp;
                            <?php echo anchor('user/project_details/'.$value->ID,'<span class="glyphicon glyphicon-resize-full" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="More"></span>'); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>    
</div>
