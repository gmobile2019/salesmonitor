
<div style="padding-top:10px;padding-bottom:20px; " class="container-fluid">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/create_projects/'.$id); 
                ?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                        <h6 class="well">Basic Details</h6>
                        <div class="form-group row">
                            <label for="oppo_name" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Project Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="oppo_name" id="oppo_name" placeholder="Project Name" value="<?php echo $id != null?$project->NAME:set_value('oppo_name'); ?>" />
                                <?php echo form_error('oppo_name'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Project Category &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                 <select name="category" id="category" class="form-control" >
                                    <option></option>
                                     <?php foreach($bcategories as $key=>$value){ ?>
                    
                                    <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project->CATEGORY))?'selected="selected"':set_select('category',$value->ID); ?>><?php echo $value->CATEGORY; ?></option>

                                    <?php } ?>

                                </select>
                                <?php echo form_error('category'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="org" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Organization &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="org" id="org" placeholder="Organisation" value="<?php echo $id != null?$project->ORGANIZATION:set_value('org'); ?>"/>

                                    <?php echo form_error('org'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Description&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <textarea class="form-control " name="description" id="description"><?php echo $id != null?$project->DESCRIPTION:set_value('description'); ?></textarea>
                                <?php echo form_error('description'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                        <h6 class="well">Product/Service Details</h6>
                        <div class="col-xs-12 col-sm-8 col-md-12 col-lg-12">
                        <?php foreach($prd_srv as $key=>$value){ ?>
                        
                            <label class="checkbox">
                                  <input type="checkbox" name="product[]" value="<?php echo $value->ID ?>" <?php echo $id <> null && in_array($value->ID, $project_products)?'checked="checked"':  set_checkbox('product', $value->ID); ?>>
                                <?php echo $value->NAME; ?>
                            </label>
                        
                              
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                        <h6 class="well">Contract Details</h6>
                        <div class="form-group row">
                            <label for="contract_val" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Total Value &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="contract_val" id="contract_val" placeholder="Total Value" value="<?php echo $id != null?$project->TOTALVALUE:set_value('contract_val'); ?>" />
                                <?php echo form_error('contract_val'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="perfomance_time" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Perfomance Duration &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <select name="perfomance_time" id="perfomance_time" class="form-control" >
                                    <option></option>
                                    <?php foreach($perfomance_durations as $key=>$value){ ?>

                                    <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project->PERFOMANCEDURATION))?'selected="selected"':set_select('perfomance_time',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                        <?php } ?>

                                </select>
                                    <?php echo form_error('perfomance_time'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year_one" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Expected Year 1 &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="year_one" id="year_one" placeholder="Year 1" value="<?php echo $id != null?$project->YEAR1:set_value('year_one'); ?>" />
                                <?php echo form_error('year_one'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year_two" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Expected Year 2</label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="year_two" id="year_two" placeholder="Year 2" value="<?php echo $id != null?$project->YEAR2:set_value('year_two'); ?>" />
                                <?php echo form_error('year_two'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year_three" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Expected Year 3</label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="year_three" id="year_three" placeholder="Year 3" value="<?php echo $id != null?$project->YEAR3:set_value('year_three'); ?>" />
                                <?php echo form_error('year_three'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year_four" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Expected Year 4</label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input type="text" class="form-control " name="year_four" id="year_four" placeholder="Year 4" value="<?php echo $id != null?$project->YEAR4:set_value('year_four'); ?>" />
                                <?php echo form_error('year_four'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                        <h6 class="well">Additional Details</h6>
                        <div class="form-group row">
                            <label for="salesrep" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Sales Representative &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <select name="salesrep" id="perfomance_time" class="form-control" >
                                    <option></option>
                                    <?php foreach($users as $key=>$value){ ?>

                                    <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project->SALESREP))?'selected="selected"':set_select('salerep',$value->ID); ?>><?php echo $value->FIRST_NAME.' '.$value->MIDDLE_NAME.' '.$value->LAST_NAME; ?></option>

                                        <?php } ?>

                                </select>
                                    <?php echo form_error('salesrep'); ?>
                            </div>
                        </div>
                       
                </div>
                <div class="form-group ">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Project</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
                </div>
</div>
