
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/add_contacts/'.$id); 
                ?>
                        <div class="form-group row">
                            <label for="project" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Project&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <select name="project" id="project" class="form-control" >
                                <option></option>
                                <?php foreach($projects as $key=>$value){ ?>

                                <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project_contacts[0]->PROJECTID))?'selected="selected"':set_select('project',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                    <?php } ?>

                            </select>
                                <?php echo form_error('project'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" class="form-control " name="name" id="name" placeholder="Name" value="<?php echo $id != null?$project_contacts[0]->NAME:set_value('name'); ?>" />
                                <?php echo form_error('name'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Title &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" class="form-control " name="title" id="title" placeholder="Title" value="<?php echo $id != null?$project_contacts[0]->TITLE:set_value('title'); ?>"/>

                                    <?php echo form_error('title'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Phone &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" class="form-control " name="phone" id="phone" placeholder="Phone" value="<?php echo $id != null?$project_contacts[0]->PHONE:set_value('phone'); ?>"/>

                                    <?php echo form_error('phone'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" class="form-control " name="email" id="email" placeholder="Email" value="<?php echo $id != null?$project_contacts[0]->EMAIL:set_value('email'); ?>"/>

                                    <?php echo form_error('email'); ?>
                            </div>
                        </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Contact</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
