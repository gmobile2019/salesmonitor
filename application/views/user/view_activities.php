
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
           $("#start" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
    });
</script>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('user/view_activities',$attributes); 
                ?>
      <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label class="sr-only" for="name"></label>
                <input type="text" class="form-control " name="name" id="name" placeholder="Project Name" value="<?php echo $name; ?>" />
           </div>
      </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="org"></label>
            <input type="text" class="form-control " name="org" id="org" placeholder="Organization" value="<?php echo $org; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="close"></label>
            <select name="close" id="close" class="form-control " >
                    <option value=""></option>
                    <option value="20" <?php echo $close== 20?"selected='selected'":"" ?>>20%</option>
                    <option value="40" <?php echo $close== 40?"selected='selected'":"" ?>>40%</option>
                    <option value="60" <?php echo $close== 60?"selected='selected'":"" ?>>60%</option>
                    <option value="80" <?php echo $close== 80?"selected='selected'":"" ?>>80%</option>
                    <option value="100" <?php echo $close== 100?"selected='selected'":"" ?>>100%</option>
            </select>
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="stage"></label>
            <select name="stage" id="stage" class="form-control " >
                <option  value=""></option>
                <option value="0" <?php echo $stage == 0?"selected='selected'":"" ?>>0</option>
                <option value="1" <?php echo $stage == 1?"selected='selected'":"" ?>>1</option>
                <option value="2" <?php echo $stage == 2?"selected='selected'":"" ?>>2</option>
                <option value="3" <?php echo $stage == 3?"selected='selected'":"" ?>>3</option>
                <option value="4" <?php echo $stage == 4?"selected='selected'":"" ?>>4</option>
            </select>
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="start"></label>
            <input type="text" class="form-control " name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="end"></label>
            <input type="text" class="form-control " name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
    </div>
    <div class="form-group col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <button type="submit" class="btn btn-success">Search</button>
    </div>
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Project Name</th>
                <th style="text-align:center;width:200px">Organization</th>
                <th style="text-align:center;width:300px">Close (%)</th>
                <th style="text-align:center;width:300px">Stage</th>
                <th style="text-align:center;width:300px">Action Date</th>
                <th style="text-align:center;width:200px">Comments</th>
             </tr>
        </thead>
        <tbody>
            <?php if($sales_activity != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($sales_activity as $key=>$value){
                    $duration=$this->User_model->get_duration($value->PERFOMANCEDURATION);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo anchor('user/project_details/'.$value->PROJECTID,'<span class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="More">'.$value->PNAME.'</span>'); ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->ORGANIZATION; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->CLOSE; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->STAGE; ?></td>
                        <td>&nbsp;&nbsp;<?php $dte=explode(" ",$value->ACTIONDATE);echo $dte[0] ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->COMMENTS; ?></td>
                    </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>
    
</div>
