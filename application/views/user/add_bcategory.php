
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/add_business_categories/'.$id); 
                ?>
                <div class="form-group row">
                            <label for="category" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Category &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <input type="text" class="form-control " name="category" id="category" placeholder="Category" value="<?php echo $id != null?$bcategory[0]->CATEGORY:set_value('category'); ?>" />
                                <?php echo form_error('category'); ?>
                            </div>
                        </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="description" id="description"><?php echo set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Category</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
