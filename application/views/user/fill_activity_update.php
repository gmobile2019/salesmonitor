<script type="text/javascript">
    $(document).ready(function(){
        $("#duedate" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#completiondate" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        
    });
    
</script>
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/fill_activity_update/'); 
                ?>
                
                <div class="form-group row">
                    <label for="project" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Project&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="project" id="project" value="<?php echo set_value('project'); ?>" />
                        <?php echo form_error('project'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tasktitle" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Task Title&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="tasktitle" id="tasktitle" value="<?php echo set_value('tasktitle'); ?>" />
                        <?php echo form_error('tasktitle'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="assigned" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Assigned To&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="assigned" id="assigned" value="<?php echo set_value('assigned'); ?>" />
                        <?php echo form_error('assigned'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Status&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select class="form-control " name="status" id="status">
                            <option value="Complete" <?php set_select('status', 'Complete')?>>Complete</option>
                            <option value="Incomplete" <?php set_select('status', 'Complete')?>>Incomplete</option>
                        </select>
                        <?php echo form_error('status'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="activities" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Activities&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="activities" id="activities"><?php echo set_value('activities','n/a'); ?></textarea>
                        <?php echo form_error('activities'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pendings" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Pending&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="pendings" id="pendings"><?php echo set_value('pendings','n/a'); ?></textarea>
                        <?php echo form_error('pendings'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="projections" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Projections&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="projections" id="projections"><?php echo set_value('projections','n/a'); ?></textarea>
                        <?php echo form_error('projections'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="duedate" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Due Date&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="duedate" id="duedate" value="<?php echo set_value('duedate'); ?>" />
                        <?php echo form_error('duedate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="completiondate" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Completion Date&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control " name="completiondate" id="completiondate" value="<?php echo set_value('completiondate'); ?>" />
                        <?php echo form_error('completiondate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="comments" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Comments&nbsp;&nbsp;</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea class="form-control " name="comments" id="comments"><?php echo set_value('comments','n/a'); ?></textarea>
                        <?php echo form_error('comments'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save Data</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>