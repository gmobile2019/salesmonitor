<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('user/view_address',$attributes); 
                ?>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="name"></label>
            <input type="text" class="form-control " name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="org"></label>
            <input type="text" class="form-control " name="org" id="org" placeholder="Organization" value="<?php echo $org; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <label class="sr-only" for="phone"></label>
            <input type="text" class="form-control " name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <button type="submit" class="btn btn-success">Search</button>
    </div>
        
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:400px">Name</th>
                <th style="text-align:center;width:400px">Organization</th>
                <th style="text-align:center;width:300px">Phone 1</th>
                <th style="text-align:center;width:300px">Phone 2</th>
                <th style="text-align:center;width:300px">Phone 3</th>
                <th style="text-align:center;width:300px">Email</th>
                <th style="text-align:center;width:300px">Phy Address</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($address != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($address as $key=>$value){
                   ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->ORGANIZATION; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->PHONE1; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->PHONE2; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->PHONE3; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->EMAIL; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->PHY_ADDRESS; ?></td>
                        <td style="text-align: center">
                            <?php echo anchor('user/fill_address/'.$value->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>')."&nbsp;&nbsp;".anchor('user/remove_address/'.$value->ID,'<span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Delete"></span>'); ?>
                        </td>
                    </tr>  
                <?php }
                }else{ ?>
                <tr>
                    <td colspan="9" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>
    
</div>
