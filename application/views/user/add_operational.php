
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/add_operational_details/'); 
                ?>
                        <div class="form-group row">
                            <label for="project" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Project&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                            <select name="project" id="project" class="form-control" >
                                <option></option>
                                <?php foreach($projects as $key=>$value){ ?>

                                <option value="<?php echo $value->ID; ?>" <?php echo ($id != null && trim($value->ID) == trim($project_contacts[0]->PROJECTID))?'selected="selected"':set_select('project',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                    <?php } ?>

                            </select>
                                <?php echo form_error('project'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="risks" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Risks&nbsp;&nbsp;</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <textarea class="form-control " name="risks" id="risks"><?php echo set_value('risks'); ?></textarea>
                                <?php echo form_error('risks'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="competition" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Competition&nbsp;&nbsp;</label>
                            <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                                <textarea class="form-control " name="competition" id="competition"><?php echo set_value('competition'); ?></textarea>
                                <?php echo form_error('competition'); ?>
                            </div>
                        </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
