
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
           $("#start" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
    });
</script>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('user/view_business_categories',$attributes); 
                ?>
      <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
             <label class="sr-only" for="category"></label>
             <input type="text" class="form-control " name="category" id="category" placeholder="Category" value="<?php echo $category; ?>" />
     </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <button type="submit" class="btn btn-success">Search</button>
    </div>
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:300px">Category</th>
                <th style="text-align:center;width:300px">Description</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($bcategories != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($bcategories as $key=>$value){
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->CATEGORY; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->DESCRIPTION; ?></td>
                        <td style="text-align: center">
                            <?php echo anchor('user/add_business_categories/'.$value->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                        </td>
                    </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="4" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>
    
</div>
