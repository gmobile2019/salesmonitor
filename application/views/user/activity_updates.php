
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
           $("#start" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
    });
</script>
<div style="padding-left:0px;padding-top: 20px" class="row">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('user/activity_updates',$attributes); 
                ?>
      
    <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <label class="sr-only" for="user"></label>
            <select name="user" id="user" class="form-control " >
                    <option value=""></option>
                    <?php foreach($users as $key=>$value){ ?>
                    
                     <option value="<?php echo $value->ID; ?>" <?php echo $user== $value->ID ?"selected='selected'":"" ?>><?php echo $value->FIRST_NAME.' '.$value->LAST_NAME?></option>   
                    
                    <?php }?>
            </select>
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <label class="sr-only" for="project"></label>
            <input type="text" class="form-control " name="project" id="project" placeholder="Project" value="<?php echo $project; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <label class="sr-only" for="start"></label>
            <input type="text" class="form-control " name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <label class="sr-only" for="end"></label>
            <input type="text" class="form-control " name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
    </div>
    <div class="form-group col-sm-offset-3 col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <button type="submit" class="btn btn-success">Search</button>
    </div>
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:300px">Project</th>
                <th style="text-align:center;width:300px">Task Title</th>
                <th style="text-align:center;width:200px">Status</th>
                <th style="text-align:center;width:300px">Timestamp</th>
                <th style="text-align:center;width:200px">Member</th>
             </tr>
        </thead>
        <tbody>
            <?php if($activity_updates != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($activity_updates as $key=>$value){
                    $member=$this->Superadministrator_model->get_member_info($value->createdby);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo anchor('user/activity_update_details/'.$value->id,$value->project); ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->tasktitle <> null?anchor('user/activity_update_details/'.$value->id,$value->tasktitle):""; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $member[0]->FIRST_NAME.' '.$member[0]->LAST_NAME; ?></td>
                    </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
        <div align="center">
            <?php echo $links; ?>
        </div>
    </div>
    
</div>