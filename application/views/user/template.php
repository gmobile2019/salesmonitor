<!DOCTYPE html>
<html lang="en">
<head>
<title>GMSM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css">
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<style type="text/css">
    .background {
        background-color: #990000;
    }
</style>
</head>
<body>
    
    <nav class="navbar navbar-default">
        <div class="container-fluid background">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> 
                <img align="left" style="margin-left:5px;border-radius: 10px" src="<?php echo base_url()?>images/gmobilelogo.jpg" height="35px"/>
            </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><?php echo anchor('user/','Home');?></li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black">Projects</span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('user/create_projects','Create Project');?></li>
                  <li><?php echo anchor('user/view_projects','View Projects');?></li>
                  <li><?php echo anchor('user/add_contacts','Add Contacts');?></li>
                  <li><?php echo anchor('user/add_operational_details','Fill Risks/Competition');?></li>
                </ul>
              </li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black">Sales Activity</span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('user/fill_activities','Fill');?></li>
                  <li><?php echo anchor('user/view_activities','View');?></li>
                </ul>
              </li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black">Address Book</span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('user/fill_address','Fill');?></li>
                  <li><?php echo anchor('user/view_address','View');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black">Business Categories</span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('user/add_business_categories','Add');?></li>
                  <li><?php echo anchor('user/view_business_categories','View');?></li>
                </ul>
              </li>
			  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black">Activity Updates</span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('user/activity_updates','View');?></li>
                  <li><?php echo anchor('user/fill_activity_update','Fill');?></li>
                </ul>
              </li>
              </ul>
              
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="color:black"><?php echo $userInfo->FIRST_NAME.' '.$userInfo->LAST_NAME; ?></span><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('user/profile','Profile');?></li>
                    <li><?php echo anchor('auth/logout','Logout');?></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      <div class="container">
            <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7">

                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                    <p style="color:black;font-weight: bolder"> User Module</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-4 col-md-4 well">
                    <?php if($side_menu <> null){?>
                    <table class="table table-hover table-bordered" style="width:100%">
                        <?php 

                        foreach($side_menu as $key=>$value){ 

                            ?>
                        <tr>
                            <td>
                                <?php echo anchor($value['link'],$value['title']);?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <?php } ?>
                </div>
                <div class="col-xs-7 col-sm-8 col-md-8">
                    <div style="padding-left:0px" class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-primary"><?php echo $title; ?></button>
                    </div>
                     <?php $this->load->view($content); ?>
                </div>
        </div>
    </div>
</body>
</html>