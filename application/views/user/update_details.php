
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-top:10px" class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="display_data">
        <table class="table table-condensed table-hover table-striped table-bordered">
            <tbody>
                <tr>
                    <td>&nbsp;&nbsp;Project</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->project; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Task Title</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->tasktitle; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Assigned To</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->assigned; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Status</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->status; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Activities</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->activities; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Pending</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->pendings; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Projections</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->projections; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Due Date</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->duedate; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Completion Date</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->completiondate; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Comments</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->comments; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Timestamp</td>
                    <td>&nbsp;&nbsp;<?php echo $activity_updates[0]->createdon; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Member</td>
                    <td>&nbsp;&nbsp;<?php $member=$this->Superadministrator_model->get_member_info($activity_updates[0]->createdby);echo $member[0]->FIRST_NAME.' '.$member[0]->LAST_NAME; ?></td>
                </tr>
               </tbody>
        </table>
    </div>
    
</div>