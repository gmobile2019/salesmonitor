<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-top:10px;padding-bottom:20px; " class="container-fluid" id="display_data">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
                <div class="row">
                    <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                       <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Basic Details</th>
                                    <th style="text-align:center;"><?php echo $project->STATUS ==0?anchor('user/create_projects/'.$project->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'):""; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>&nbsp;&nbsp;Project Name </td>
                                    <td>&nbsp;&nbsp;<?php echo $project->NAME; ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;Project Category </td>
                                    <td>&nbsp;&nbsp;<?php $bcateg=$this->User_model->business_category($project->CATEGORY); echo $bcateg[0]->CATEGORY; ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;ORGANIZATION</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->ORGANIZATION; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;DESCRIPTION</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->DESCRIPTION; ?></td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Product/Service Details</th>
                                    <th style="text-align:center;"><?php echo $project->STATUS ==0?anchor('user/create_projects/'.$project->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'):""; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($prd_srv as $key=>$value){ 
                                    $prodsrv=$this->Superadministrator_model->products_services($value->PRODUCTID);
                                    ?>
                                
                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;<?php echo $prodsrv[0]->NAME; ?> </td>
                                </tr> 
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                       <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="4">Contact Details</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">Name</th>
                                    <th style="text-align:center;">Title</th>
                                    <th style="text-align:center;">Phone</th>
                                    <th style="text-align:center;">Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach($contacts as $key=>$value){ ?>
                                    
                                <tr>
                                    <td>&nbsp;&nbsp;<?php echo $project->STATUS ==0?anchor('user/add_contacts/'.$value->ID,'<span class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit">'.$value->NAME.'</span>'):$value->NAME; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $value->TITLE; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $value->PHONE; ?></td>
                                    <td>&nbsp;&nbsp;<?php echo $value->EMAIL; ?></td>
                                </tr>
                                <?php } ?>
                                  
                                  
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">Operational Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        Risks 
                                    </td>
                                    <td>
                                        <ol>
                                            
                                        
                                        <?php foreach($ops as $key=>$value){ 
                                            if($value->RISKS <> null){
                                            ?>
                                            
                                            <li>
                                                <?php echo $project->STATUS ==0?anchor('user/remove_risk/'.$value->ID.'/'.$value->PROJECTID,'<span class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Remove Risk">'.$value->RISKS.'</span>'):$value->RISKS;  ?>
                                            </li>
                                        <?php }
                                             } ?>
                                        </ol>
                                    </td>   
                                </tr> 
                                <tr>
                                    <td>
                                        Competition 
                                    </td>
                                    <td>
                                        <ol>
                                            
                                        
                                        <?php foreach($ops as $key=>$value){ 
                                             if($value->COMPETITION <> null){
                                            ?>
                                            <li>
                                                <?php echo $project->STATUS ==0?anchor('user/remove_competition/'.$value->ID.'/'.$value->PROJECTID,'<span class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Remove Competition">'.$value->COMPETITION.'</span>'):$value->COMPETITION;  ?>
                                            </li>
                                             <?php }
                                               } ?>
                                        </ol>
                                    </td>   
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Contract Details</th>
                                    <th style="text-align:center;"><?php echo $project->STATUS ==0?anchor('user/create_projects/'.$project->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'):""; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>&nbsp;&nbsp;TOTAL VALUE</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->TOTALVALUE; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;PERFOMANCE DURATION</td>
                                    <td>&nbsp;&nbsp;<?php $duration=$this->User_model->get_duration($project->PERFOMANCEDURATION);echo $duration[0]->NAME; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;YEAR ONE</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->YEAR1; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;YEAR TWO</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->YEAR2; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;YEAR THREE</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->YEAR3; ?></td>
                                </tr>  
                                <tr>
                                    <td>&nbsp;&nbsp;YEAR FOUR</td>
                                    <td>&nbsp;&nbsp;<?php echo $project->YEAR4; ?></td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                     <div class="col-md-12 col-md-6 col-sm-6 col-lg-6">
                        <table class="table table-condensed table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Additional Details</th>
                                    <th style="text-align:center;"><?php echo $project->STATUS ==0?anchor('user/create_projects/'.$project->ID,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'):""; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>&nbsp;&nbsp;SALES REPRESENTATIVE</td>
                                    <td>&nbsp;&nbsp;<?php $rep=$this->Superadministrator_model->get_users($project->SALESREP);echo $rep[0]->FIRST_NAME.' '.$rep[0]->MIDDLE_NAME.' '.$rep[0]->LAST_NAME; ?></td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                </div>
</div>
