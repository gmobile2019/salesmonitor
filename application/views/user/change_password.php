
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('user/change_password/'); 
                ?>
                <div class="form-group row">
                    <label for="password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Current Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="cur_password" class="form-control " name="cur_password" id="cur_password" placeholder="Current Password"/>
                        <?php echo form_error('cur_password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">New Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="password" id="password" placeholder="New Password"/>
                        <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="conf_password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Confirm New Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="conf_password" id="conf_password" placeholder="Confirm New Password"/>
                        <?php echo form_error('conf_password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Change Password</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
