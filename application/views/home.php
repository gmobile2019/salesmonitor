<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GMSM</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.min.css">

        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.min.js" ></script>
    </head>
    <body style="background-color:#990000">
        
        <div class="container-fluid">
           <div class="row">
                
                <div class=" col-sm-3 col-md-3" ></div>
                <div class="col-xs-12 col-sm-9 col-md-9" >
                    <div class="row" style="padding-top:80px;">
                        <div>
                            <img align="left" style="margin-left:-15px;border-radius: 10px" src="<?php echo base_url()?>images/gmobilelogo.jpg" height="70px"/>
                            <span style="color:#003300;font-size:200%;font-weight: bold;font-style: italic">G-MOBILE SALES MONITOR</span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 well" style="border-radius:15px">
                            <div>
                                <?php echo $message; ?>
                            </div>
                            <form class="form-horizontal" role="form" style="padding-top:50px;padding-bottom:50px;padding-left:20px;padding-right:20px" method="POST" action="<?php echo base_url().'index.php/auth/login'; ?>">
                                    <div class="form-group">
                                        <label for="username" class="col-xs-12 col-sm-2 col-md-2 control-label">Username</label>
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                        <input type="text" class="form-control" id="username" placeholder="username" name="identity">
                                        <?php echo form_error('identity'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-xs-12 col-sm-2 col-md-2 control-label">Password</label>
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                        <input type="password" class="form-control" id="password" placeholder="password" name="password">
                                        <?php echo form_error('password'); ?>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-xs-10 col-sm-10 col-md-10">
                                        <button type="submit" class="btn btn-success">Sign in</button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6"></div>
                </div>
            </div>
        </div>
        
    </body>
</html>